package dashboard.app.util;

/**
 * Created by Mario on 28/02/2015.
 */
public interface FileType {

    public String[] getFileExtension();

    public FileTypeFilter getFileTypeFilter();

    public String getDescription();

//    public FileType fromFileExtension(String fileExtension);

}
