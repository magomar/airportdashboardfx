package dashboard.app.util;

import java.time.LocalDateTime;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mario on 19/02/2015.
 */
public class FixedExecutionRunnable implements Runnable {
    private final Runnable delegate;
    private volatile ScheduledFuture<?> self;
    private LocalDateTime maxRunTime;

    public FixedExecutionRunnable(Runnable delegate, LocalDateTime maxRunTime) {
        this.delegate = delegate;
        this.maxRunTime = maxRunTime;
    }

    public void run() {
        delegate.run();
        if (LocalDateTime.now().isAfter(maxRunTime)) {
            self.cancel(false);
        }
    }

    public void runUntil(ScheduledExecutorService executor, long initialDelay, long period, TimeUnit unit) {
        self = executor.scheduleAtFixedRate(this, initialDelay, period, unit);
    }

}