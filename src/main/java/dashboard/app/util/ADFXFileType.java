package dashboard.app.util;

/**
 * Created by Mario on 28/02/2015.
 */
public enum ADFXFileType implements FileType {
    FLIGHTS(new String[]{".xml", ".zip"}, "Flight Collection");

    private final String[] fileExtension;
    private final String description;
    private final FileTypeFilter fileTypeFilter;

    ADFXFileType(final String[] fileExtension, final String description) {
        this.fileExtension = fileExtension;
        this.description = description;
        this.fileTypeFilter = new FileTypeFilter(this);
    }

    @Override
    public String[] getFileExtension() {
        return fileExtension;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public FileTypeFilter getFileTypeFilter() {
        return fileTypeFilter;
    }

//    public ADFXFileType fromFileExtension(String extension) {
//        if (extension != null) {
//            for (ADFXFileType fileType : ADFXFileType.values()) {
//                if (extension.equalsIgnoreCase(fileType.fileExtension)) {
//                    return fileType;
//                }
//            }
//        }
//        return null;
//    }
}

