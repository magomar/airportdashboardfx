package dashboard.app.util;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Heine on 10/28/2014.
 */
@XmlRootElement(name = "settings")
public class Settings {

    private static Settings settings;
    private static String airportShown;
    private static List<String> airportSelected = new ArrayList<>();
    private static Double windowWidth;
    private static Double windowHeight;


    private Settings() {
    }

    public static Settings getInstance() {
        if (settings == null) settings = new Settings();
        return settings;
    }

    public static void save() {
        FileIO.saveSettings();
    }

    public static void load() {
        FileIO.loadSettings();
    }


    @XmlElement(name = "window-width", required = true)
    public Double getWindowWidth() {
        return windowWidth;
    }

    public void setWindowWidth(Double windowWidth) {
        this.windowWidth = windowWidth;
    }

    @XmlElement(name = "window-height", required = true)
    public Double getWindowHeight() {
        return windowHeight;
    }

    public void setWindowHeight(Double windowHeight) {
        this.windowHeight = windowHeight;
    }

    @XmlElement(name = "airport-shown")
    public String getAirportShown() {
        return airportShown;
    }

    public void setAirportShown(String airportShown) {
        Settings.airportShown = airportShown;
    }

    @XmlElement(name = "airport-selected")
    public List<String> getAirportSelected() {
        return airportSelected;
    }
}
