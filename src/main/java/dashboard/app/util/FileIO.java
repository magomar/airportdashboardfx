package dashboard.app.util;

import dashboard.app.messages.MessageLogLevel;
import dashboard.app.messages.MessageLogger;
import dashboard.app.model.Airport;
import dashboard.app.model.AirportFlights;
import dashboard.app.model.FlightsCollection;
import javafx.stage.FileChooser;
import org.xml.sax.SAXParseException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.*;

/**
 * Created by Mario on 24/07/2014.
 */
public class FileIO {
    /**
     * File  and folder paths
     */
    public static final String SETTINGS_XML_FILE = "settings.xml";
    private static final Logger LOG = Logger.getLogger(FileIO.class.getName());
    private static final boolean IS_DEPLOYED;

    static {
        Path userPath = FileSystems.getDefault().getPath(System.getProperty("user.dir"));
        IS_DEPLOYED = userPath.endsWith("bin");
        LOG.log(Level.INFO, "User dir = " + userPath.toString() + " -- Is deployed? " + IS_DEPLOYED);
    }

    /**
     * File filters
     */
    public static final FileChooser.ExtensionFilter[] FILECHOOSER_FILTERS = {
            new FileChooser.ExtensionFilter("XML (*.xml)", "*.xml"),
            new FileChooser.ExtensionFilter("All (*.*)", "*.*")
    };

    /**
     * JAXB paths
     */
    private static final String JAXB_CONTEXT_PATH = "dashboard.data.jaxb";
    private static JAXBContext JAXB_CONTEXT;
    private static Marshaller MARSHALLER;
    private static Unmarshaller UNMARSHALLER;

    static {
        try {
            JAXB_CONTEXT = JAXBContext.newInstance(JAXB_CONTEXT_PATH);
            UNMARSHALLER = JAXB_CONTEXT.createUnmarshaller();
            MARSHALLER = JAXB_CONTEXT.createMarshaller();
            MARSHALLER.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    private FileIO() {
    }


    public static boolean isDeployed() {
        return IS_DEPLOYED;
    }

    /**
     * **** FILES  ******
     */

    public static List<File> listFiles(File directory, FilenameFilter filter, boolean recursively) {
        List<File> files = new ArrayList<>();
        if (directory.isDirectory()) {
            File[] entries = directory.listFiles();
            for (File entry : entries) {
                if (filter == null || filter.accept(directory, entry.getName())) {
                    files.add(entry);
                }
                if (recursively && entry.isDirectory()) {
                    files.addAll(listFiles(entry, filter, recursively));
                }
            }
        } else {
            LOG.severe(String.format("Error listing files. %s is not a directory", directory));
        }
        return files;
    }

    public static File getFile(String... relativePath) {
        Path path = getPath(relativePath);
        return getFileOrCreateNew(path.toString());
    }

    /**
     * Tries to create a new file in @path, if it exists then returns the file
     *
     * @param path
     * @return
     */
    private static File getFileOrCreateNew(String path) {
        File f = null;
        try {
            f = new File(path);
            boolean exists = f.exists();
            if (!exists) {
                boolean createNew = f.createNewFile();
                if (!createNew) {
                    throw new IOException();
                }
            }
        } catch (IOException e) {
            LOG.log(Level.WARNING, "Error creating: " + path, new IOException());
        }
        return f;
    }

    public static Path getPath(String... relativePath) {
        String[] paths;
        if (IS_DEPLOYED) {
            int numPaths = relativePath.length + 1;
            paths = new String[numPaths];
            paths[0] = "..";
            for (int i = 1; i < paths.length; i++) {
                paths[i] = relativePath[i - 1];
            }
        } else {
            paths = relativePath;
        }
        return FileSystems.getDefault().getPath(System.getProperty("user.dir"), paths);
    }

//    public static Path getResourcesPath(String module, String... relativePath) {
//        String[] paths = new String[relativePath.length + 2];
//        paths[0] = module;
//        paths[1] = FileIO.RESOURCES_FOLDER;
//        for (int i = 2; i < paths.length; i++) {
//            paths[i] = relativePath[i - 2];
//        }
//        return getPath(paths);
//    }


    public static void copy(File sourceFile, File targetFile) {
        try {
            Files.copy(sourceFile.toPath(), targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String removeExtention(String filePath) {
        File f = new File(filePath);
        // if it's a directory there is no extension to remove
        if (f.isDirectory()) return filePath;
        String name = f.getName();
        final int lastPeriodPos = name.lastIndexOf('.');
        if (lastPeriodPos <= 0) {
            // No period after first character - return name as it was passed in
            return filePath;
        } else {
            // Remove the last period and everything after it
            File renamed = new File(f.getParent(), name.substring(0, lastPeriodPos));
            return renamed.getPath();
        }
    }


    public static String getExtension(File file) {
        if (file.isDirectory()) return "";
        String name = file.getName();
        final int lastPeriodPos = name.lastIndexOf('.');
        if (lastPeriodPos <= 0) {
            // No period after first character - return name as it was passed in
            return "";
        } else {
            return name.substring(lastPeriodPos + 1);
        }
    }


    /**
     * **** SETTINGS  ******
     */

    public static File getNewSettingsFile() {
        File f = null;
        try {
            f = getFileOrCreateNew(FileSystems.getDefault().getPath(System.getProperty("user.dir"), FileIO.SETTINGS_XML_FILE).toString());
            FileOutputStream fos = new FileOutputStream(f, false);
            fos.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><settings><vertical-panes>true</vertical-panes><visible-source-panel>true</visible-source-panel><visible-target-panel>true</visible-target-panel><visible-toolbar>true</visible-toolbar><window-height>700.0</window-height><window-width>900.0</window-width><expanded-source-pane>true</expanded-source-pane><expanded-target-pane>false</expanded-target-pane></settings>".getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }

    public static void loadSettings() {
        try {
            File file = new File(FileSystems.getDefault().getPath(System.getProperty("user.dir"), SETTINGS_XML_FILE).toString());
            if (!file.exists()) {
                file = getNewSettingsFile();
            }
            Unmarshaller um = JAXBContext.newInstance(Settings.class).createUnmarshaller();
            um.unmarshal(file);

        } catch (JAXBException e) {
            System.out.println(e);
            if (e.getLinkedException().getClass().equals(SAXParseException.class)) {
                LOG.log(Level.WARNING, "Settings file has incorrect syntax", e);
            }
        }
    }

    public static void saveSettings() {
        try {
            Marshaller m = JAXBContext.newInstance(Settings.class).createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            m.marshal(Settings.getInstance(), getFileOrCreateNew(SETTINGS_XML_FILE));

        } catch (JAXBException e) {
            LOG.log(Level.WARNING, "Couldn't save settings", e);
        }
    }

    /******* MARSHALLING / UNMARSHALLING  *******/

    /**
     * Unmarshalls XML element from file into java object
     *
     * @param file the XML file to be unmarshalled
     * @return the object unmarshalled from the {@code file}
     */
    public static Object unmarshallXML(File file) {
        try {
            Object object = UNMARSHALLER.unmarshal(new StreamSource(file));
            return object;
        } catch (JAXBException ex) {
            LOG.log(Level.SEVERE, "Exception unmarshalling XML", ex);
            return null;
        }
    }

    /**
     * Marshalls Java object into XML file
     *
     * @param object object to be marshalled
     * @param file   file to save the marshalled object
     * @return the XML file
     */
    public static File marshallXML(Object object, File file) {
        try {
            try (FileOutputStream fos = new FileOutputStream(file)) {
                MARSHALLER.marshal(object, fos);
                return file;
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
            }
        } catch (JAXBException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static StringWriter marshallXML(Object object) {
        try (StringWriter sw = new StringWriter()) {
            MARSHALLER.marshal(object, sw);
            return sw;
        } catch (IOException | JAXBException ex) {
            LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
        }
        return null;
    }

    public static Object unmarshallZip(File file) {
        Object object = null;
        try (ZipFile zipFile = new ZipFile(file.getPath())) {
            ZipEntry zipEntry = zipFile.entries().nextElement();
            object = UNMARSHALLER.unmarshal(zipFile.getInputStream(zipEntry));
        } catch (JAXBException ex) {
            LOG.log(Level.SEVERE, "Exception unmarshalling XML", ex);
        } finally {
            return object;
        }
    }

    /**
     * Marshalls Java object in a zipped XMl file
     *
     * @param object object to be marshalled
     * @param file   non zip file to save the marshalled object
     * @return the ZIP file
     */
    public static File marshallZipped(Object object, File file) {
        File zipFile = new File(file.getAbsolutePath() + ".zip");
        try {
            try {
                FileOutputStream fos = new FileOutputStream(zipFile);
                try (ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(fos))) {
                    ZipEntry ze = new ZipEntry(file.getName());
                    zos.putNextEntry(ze);
                    MARSHALLER.marshal(object, zos);
                    return zipFile;
                } catch (IOException ex) {
                    LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
                }
            } catch (FileNotFoundException ex) {
                LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
            }

        } catch (JAXBException ex) {
            LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
        }
        return null;
    }

    /**
     * Marshalls Java object in a gzipped XMl file
     *
     * @param object object to be marshalled
     * @param file   file to save the marshalled object
     * @return the Gzip file
     */
    public static File marshallGzipped(Object object, File file) {
        File gzFile = new File(file.getAbsolutePath() + ".gz");
        try {
            try {
                FileOutputStream fos = new FileOutputStream(gzFile);
                try {
                    GZIPOutputStream gz = new GZIPOutputStream(fos);
                    MARSHALLER.marshal(object, gz);
                    return gzFile;
                } catch (IOException ex) {
                    LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
                }
            } catch (FileNotFoundException ex) {
                LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
            }

        } catch (JAXBException ex) {
            LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
        }
        return null;
    }

    public enum SaveOption {
        SINGLE_FILE,
        ONE_FILE_PER_AIRPORT,
        ONE_FILE_PER_DAY;
    }

    public enum CompressionOption {
        NO_COMPRESSION,
        ZIP,
        GZIP;
    }

    public static void saveToFile(FlightsCollection flightsCollection, SaveOption saveOption, CompressionOption compressionOption) {
        switch (saveOption) {
            case SINGLE_FILE:
                saveSingleFile(flightsCollection, compressionOption);
                break;
            case ONE_FILE_PER_AIRPORT:
                saveOneFilePerAirport(flightsCollection, compressionOption);
                break;
            case ONE_FILE_PER_DAY:
                saveOneFilePerDay(flightsCollection, compressionOption);
                break;
        }
    }

    private static void saveFile(String filename, FlightsCollection flightsCollection, CompressionOption compressionOption) {
        dashboard.data.jaxb.FlightsCollection flightsCollectionData = flightsCollection.getData();
        MessageLogger.log(FlightsCollection.class, MessageLogLevel.MEDIUM, String.format("Saving flights to %s", filename));
        File xmlFile = FileIO.getFile("data", filename);
        switch (compressionOption) {
            case NO_COMPRESSION:
                FileIO.marshallXML(flightsCollectionData, xmlFile);
                break;
            case ZIP:
                FileIO.marshallZipped(flightsCollectionData, xmlFile);
                break;
            case GZIP:
                FileIO.marshallGzipped(flightsCollectionData, xmlFile);
        }

    }

    private static void saveSingleFile(FlightsCollection flightsCollection, CompressionOption compressionOption) {
        String filename = "flights_"
                + flightsCollection.getFrom().toLocalDate() + '_'
                + flightsCollection.getTo().toLocalDate() + ".xml";
        saveFile(filename, flightsCollection, compressionOption);
    }

    private static void saveOneFilePerAirport(FlightsCollection flightsCollection, CompressionOption compressionOption) {
        for (Map.Entry<Airport, AirportFlights> entry : flightsCollection.getAirportFlights().entrySet()) {
            Airport airport = entry.getKey();
            AirportFlights airportFlights = entry.getValue();
            FlightsCollection fc = new FlightsCollection();
            fc.getAirportFlights().put(airport, airportFlights);
            String filename = airport.getCode() + "_"
                    + flightsCollection.getFrom().toLocalDate() + '_'
                    + flightsCollection.getTo().toLocalDate() + ".xml";
            saveFile(filename, flightsCollection, compressionOption);
        }
    }

    private static void saveOneFilePerDay(FlightsCollection flightsCollection,CompressionOption compressionOption) {
        Map<LocalDate, FlightsCollection> flightsCollectionByDate = flightsCollection.getFlightsCollectionByDate();
        for (Map.Entry<LocalDate, FlightsCollection> entry : flightsCollectionByDate.entrySet()) {
            LocalDate date = entry.getKey();
            FlightsCollection fc = entry.getValue();
            String filename = "day-flights_"
                    + date.toString() + ".xml";
            saveFile(filename, fc, compressionOption);
        }
    }


//    /**
//     * Unmarshalls Json element of type {@code c} from the {@code file}.
//     *
//     * @param c    the class of object to be unmarshalled
//     * @param file the Json file containing the marshalled object
//     * @return the object of type {@code T} from the {@code file}
//     */
//    public static <T> T unmarshallJson(File file, Class<T> c) {
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
//        T object = null;
//        try {
//            object = mapper.readValue(file, c);
//        } catch (IOException ex) {
//            LOG.log(Level.SEVERE, "Exception unmarshalling Json", ex);
//        }
//        return object;
//    }
//
//    /**
//     * Marshalls Java object into a Json file
//     *
//     * @param object object to be marshalled
//     * @param file   file to save the marshalled object
//     * @return
//     */
//    public static File marshallJson(Object object, File file) {
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
//        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
//        try {
//            writer.writeValue(file, object);
//            return file;
//        } catch (IOException ex) {
//            LOG.log(Level.SEVERE, "Exception marshalling Json", ex);
//        }
//        return null;
//    }

}
