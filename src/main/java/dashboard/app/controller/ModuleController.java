package dashboard.app.controller;

import javafx.fxml.Initializable;
import javafx.scene.Parent;


/**
 * Created by Mario on 01/03/2015.
 */
public interface ModuleController<T extends Parent> extends Initializable {
    T getContentPane();
}
