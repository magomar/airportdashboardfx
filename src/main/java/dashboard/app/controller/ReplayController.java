package dashboard.app.controller;

import dashboard.app.model.*;
import dashboard.app.replay.ClockListener;
import dashboard.app.replay.SimulationClock;
import dashboard.app.util.DateTimeUtils;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by Mario on 27/02/2015.
 */
public class ReplayController extends AbstractModuleController<VBox> implements ClockListener {
    @FXML
    private ComboBox<Airport> airportComboBox;

    @FXML
    private TextField fromTextField;

    @FXML
    private TextField toTextField;

    @FXML
    private TextField dateTextField;

    @FXML
    private TextField timeTextField;

    @FXML
    private Slider speedSlider;

    @FXML
    private TextField speedTextField;

    @FXML
    private FlightsController departuresController;

    @FXML
    private FlightsController arrivalsController;

    private ObservableList<Airport> airports = FXCollections.observableArrayList();
    private SimulationClock engine;
    private FlightsCollection flightsCollection;
    private LocalDateTime currentDateTime;
    private Map<Airport, AirportReplayModel> replayModels = new HashMap<>();
    private StringConverter<Double> stringConverter = new StringConverter<Double>() {
        @Override
        public String toString(Double object) {
            return String.format("%.0f", object);
        }

        @Override
        public Double fromString(String string) {
            return Double.valueOf(string);
        }
    };

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        speedSlider.setValue(1);
        speedTextField.textProperty().bindBidirectional(speedSlider.valueProperty(), (StringConverter) stringConverter);
        speedSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if ((Double) newValue < 1.0) speedSlider.setValue(1);
        });
    }

    public void setDataModel(FlightsCollection flightsCollection) {
        if (this.flightsCollection != flightsCollection) {
            this.flightsCollection = flightsCollection;
            fromTextField.setText(DateTimeUtils.format(flightsCollection.getFrom()));
            toTextField.setText(DateTimeUtils.format(flightsCollection.getTo()));
            engine = new SimulationClock(flightsCollection.getFrom(), flightsCollection.getTo(), speedSlider.valueProperty());
            engine.addListener(this);
            airports.addAll(flightsCollection.getAirports());
            Collections.sort(airports, (o1, o2) -> o1.getName().compareTo(o2.getName()));
            airportComboBox.setItems(airports);
            for (AirportFlights airportFlights : flightsCollection.getAirportFlights().values()) {
                AirportReplayModel airportReplayModel = new AirportReplayModel(airportFlights);
                replayModels.put(airportFlights.getAirport(), airportReplayModel);
            }
            airportComboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                AirportReplayModel airportReplayModel = replayModels.get(newValue);
                engine.removeListener(replayModels.get(oldValue));
                engine.addListener(replayModels.get(newValue));
                arrivalsController.setVisibleFlights(airportReplayModel.getVisibleArrivals());
                departuresController.setVisibleFlights(airportReplayModel.getVisibleDepartures());
            });
            airportComboBox.getSelectionModel().selectFirst();
        }
    }


    @FXML
    private void play() {
        engine.play();
    }

    @FXML
    private void pause() {
        engine.pause();
    }

    @Override
    public void update(LocalDateTime dateTime) {
        Platform.runLater(() -> {
            dateTextField.setText(DateTimeUtils.format(dateTime.toLocalDate()));
            timeTextField.setText(DateTimeUtils.format(dateTime.toLocalTime()));
        });
    }


}

