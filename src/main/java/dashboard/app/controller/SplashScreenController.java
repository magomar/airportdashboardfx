package dashboard.app.controller;

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.Transition;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by Mario on 27/02/2015.
 */
public class SplashScreenController extends AbstractModuleController<StackPane> {

    @FXML
    private ImageView splashImage1;

    @FXML
    private ImageView splashImage2;

    private Image[] images;
    private Thread th;
    private BooleanProperty running = new SimpleBooleanProperty(false);
    private Task<Integer> task;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        splashImage1.fitWidthProperty().bind(getContentPane().widthProperty());
        splashImage1.fitHeightProperty().bind(getContentPane().heightProperty());
        splashImage2.fitWidthProperty().bind(getContentPane().widthProperty());
        splashImage2.fitHeightProperty().bind(getContentPane().heightProperty());
        images = new Image[SPLASH_IMAGES.length];
        for (int i = 0; i < images.length; i++) {
            images[i] = new Image(getClass().getClassLoader().getResourceAsStream("images/splash/" + SPLASH_IMAGES[i]));
        }

        splashImage1.setImage(images[0]);
        splashImage2.setImage(images[1]);
        splashImage1.setOpacity(1);
        splashImage2.setOpacity(0);
        Transition[] transitions = new Transition[images.length];
        for (int i = 0; i < transitions.length - 1; i++) {
            transitions[i] = createTransition(i);
        }

        final int totalIterations = transitions.length - 1;
        task = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                int iterations;
                for (iterations = 0; iterations < totalIterations; iterations++) {
                    if (isCancelled()) {
                        updateMessage("Cancelled");
                        break;
                    }
                    final int finalIterations = iterations;
                    Thread.currentThread().sleep(3000);
                    Platform.runLater(transitions[finalIterations]::play);
                    Thread.currentThread().sleep(3000);
                    updateMessage("Iteration " + iterations);
                    updateProgress(iterations, totalIterations);
                }
                return iterations;
            }
        };
        running.addListener((observable, oldValue, newValue) -> {
            if (newValue && !oldValue) {
                th = new Thread(task);
                th.setDaemon(true);
                th.start();
            } else if (!newValue && oldValue) {
                th = null;
            }
        });
        running.set(true);
    }

    public void resume() {
        running.set(true);
    }

    public void pause() {
        running.setValue(false);
    }

    private Transition createTransition(final int order) {
        FadeTransition fadeOutTransition
                = new FadeTransition(Duration.seconds(3), splashImage1);
        fadeOutTransition.setFromValue(1.0);
        fadeOutTransition.setToValue(0.0);

        FadeTransition fadeInTransition
                = new FadeTransition(Duration.seconds(3), splashImage2);
        fadeInTransition.setFromValue(0.0);
        fadeInTransition.setToValue(1.0);
        fadeInTransition.setOnFinished((actionEvent) -> {
            splashImage1.setImage(splashImage2.getImage());
            splashImage1.setOpacity(1);
            splashImage2.setOpacity(0);
            if (order + 2 < images.length)
                splashImage2.setImage(images[order + 2]);
            else splashImage2.setImage(images[0]);
        });

        ParallelTransition transition = new ParallelTransition(getContentPane(), fadeOutTransition, fadeInTransition);
        return transition;
    }

    private static final Image[] getSplashImages() throws IOException {
        CodeSource src = SplashScreenController.class.getProtectionDomain().getCodeSource();
        List<String> list = new ArrayList<>();
        if (src != null) {
            URL jar = src.getLocation();
            ZipInputStream zip = new ZipInputStream(jar.openStream());
            ZipEntry ze = null;
            while ((ze = zip.getNextEntry()) != null) {
                String entryName = ze.getName();
                if (entryName.startsWith("image") && entryName.endsWith(".jpg")) {
                    list.add(entryName);

                }
            }
            Image[] images = new Image[list.size()];
            for (int i = 0; i < images.length; i++) {
                images[i] = new Image(SplashScreenController.class.getClassLoader().getResourceAsStream("images/splash/" + list.get(i)));
            }
            return images;
        }
        return null;
    }

    private static final String[] SPLASH_IMAGES = {"Image01.jpg",
            "Image02.jpg",
            "Image03.jpg",
            "Image04.jpg",
            "Image05.jpg",
            "Image06.jpg",
            "Image07.jpg",
            "Image08.jpg",
            "Image09.jpg",
            "Image10.jpg",
            "Image11.jpg",
            "Image12.jpg",
            "Image13.jpg",
            "Image14.jpg",
            "Image15.jpg",
            "Image16.jpg",
            "Image17.jpg",
            "Image18.jpg",
            "Image19.jpg",
            "Image20.jpg",
            "Image21.jpg",
            "Image22.jpg",
            "Image23.jpg",
            "Image24.jpg",
            "Image25.jpg"};

}
