package dashboard.app.controller;

import dashboard.app.model.Airport;
import dashboard.app.model.AirportStats;
import dashboard.app.model.SummaryStats;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

/**
 * Created by Mario on 27/02/2015.
 */
public class SummaryStatsController extends AbstractModuleController<VBox> {

    @FXML
    private TextField fromTextField;

    @FXML
    private TableView<AirportStats> airportDetailsTable;

    @FXML
    private TableColumn airportColumn;

    @FXML
    private TableColumn numFlightsColumn;

    @FXML
    private TableColumn numInternationalFlights;

    @FXML
    private TableColumn numNationalFlightsColumn;

    @FXML
    private TableColumn flightsDayColumn;

    @FXML
    private TableColumn delayColumn;

    @FXML
    private TextField numFlightsTextField;

    @FXML
    private TextField numNationalFlightsTextField;

    @FXML
    private TextField numInternationalFlightsTextField;

    @FXML
    private TextField numAirportsTextField;

    @FXML
    private TextField toTextField;

    @FXML
    private PieChart flightsAirportPieChart;

    @FXML
    private LineChart<String, Integer> flightsDayLineChart;

    @FXML
    private LineChart<String, Integer> airportFlightsDayLineChart;

    @FXML
    private NumberAxis flightsDayLineChartNumAxis;

    @FXML
    private CategoryAxis flightsDayLineChartCatAxis;

    private SummaryStats summaryStats;

    private final static DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#0.0");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        airportFlightsDayLineChart.setVisible(false);
        airportDetailsTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        newValue.update();
                        airportFlightsDayLineChart.setData(newValue.getFlightsPerDayData());
                        airportFlightsDayLineChart.setVisible(true);
                    } else {
                        airportFlightsDayLineChart.setVisible(false);
                    }

                });
    }

    public void setDataModel(SummaryStats summaryStats) {
        if (this.summaryStats != summaryStats) {
            this.summaryStats = summaryStats;
            // Overall statistics
            numAirportsTextField.textProperty().bind(Bindings.convert(summaryStats.numAirportsProperty()));
            numFlightsTextField.textProperty().bind(Bindings.convert(summaryStats.numFlightsProperty()));
            numNationalFlightsTextField.textProperty().bind(Bindings.convert(summaryStats.numNationalFlightsProperty()));
            numInternationalFlightsTextField.textProperty().bind(Bindings.convert(summaryStats.numInternationalFlightsProperty()));
            fromTextField.textProperty().bind(Bindings.convert(summaryStats.fromProperty()));
            toTextField.textProperty().bind(Bindings.convert(summaryStats.toProperty()));
            airportDetailsTable.setItems(summaryStats.getAirportStats());
            // Detailed Airport statistics
            airportColumn.setCellValueFactory(new PropertyValueFactory<AirportStats, Airport>("airport"));
            numFlightsColumn.setCellValueFactory(new PropertyValueFactory<AirportStats, Integer>("numFlights"));
            numNationalFlightsColumn.setCellValueFactory(new PropertyValueFactory<AirportStats, Integer>("numNationalFlights"));
            numInternationalFlights.setCellValueFactory(new PropertyValueFactory<AirportStats, Integer>("numInternationalFlights"));
            flightsDayColumn.setCellValueFactory(new PropertyValueFactory<AirportStats, Double>("flightsDay"));
            flightsDayColumn.setCellFactory(COLUMN_FORMATTER);
            delayColumn.setCellValueFactory(new PropertyValueFactory<AirportStats, Double>("averageDelay"));
            delayColumn.setCellFactory(COLUMN_FORMATTER);
            // Charts
            // Line chart: flights per day
            flightsDayLineChartCatAxis.setCategories(summaryStats.getDateLabels());
            flightsDayLineChart.getData().clear();
            flightsDayLineChart.setData(summaryStats.getFlightsPerDaySeries());
            // Pie chart: flights per airport
            flightsAirportPieChart.getData().clear();
            flightsAirportPieChart.getData().addAll(summaryStats.getFlightsPerAirportData());
            airportDetailsTable.getSelectionModel().selectFirst();
        }
    }

    private static final Callback<TableColumn, TableCell> COLUMN_FORMATTER = new Callback<TableColumn, TableCell>() {
        @Override
        public TableCell call(TableColumn param) {
            TableCell cell = new TableCell<AirportStats, Double>() {
                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                }

                private String getString() {
                    String ret = "";
                    if (getItem() != null) {
                        String gi = getItem().toString();
                        ret = DECIMAL_FORMAT.format(Double.parseDouble(gi));
                    } else {
                        ret = "0.00";
                    }
                    return ret;
                }
            };
//                    cell.setStyle("-fx-alignment: top-right;");
            return cell;
        }
    };
}
