package dashboard.app.controller;

import dashboard.app.model.Flight;
import dashboard.app.model.FlightStatus;
import dashboard.app.model.SharedFlight;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collection;
import java.util.ResourceBundle;

/**
 * Created by Mario on 10/02/2015.
 */
public class FlightsController implements Initializable {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableColumn date;

    @FXML
    private TableColumn time;

    @FXML
    private TableColumn flightNumber;

    @FXML
    private TableColumn company;

    @FXML
    private TableColumn origin;

    @FXML
    private TableColumn destiny;

    @FXML
    private TableColumn terminal;

    @FXML
    private TableColumn plane;

    @FXML
    private TableColumn status;

    @FXML
    private TableColumn delay;

    @FXML
    private TableView contentPane;

    private ObservableList<Flight> flights = FXCollections.observableArrayList();


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        date.setCellValueFactory(new PropertyValueFactory<Flight, ObjectProperty<LocalDate>>("date"));
        time.setCellValueFactory(new PropertyValueFactory<Flight, ObjectProperty<LocalTime>>("time"));
        flightNumber.setCellValueFactory(new PropertyValueFactory<Flight, StringProperty>("flightNumber"));
        company.setCellValueFactory(new PropertyValueFactory<Flight, StringProperty>("company"));
        origin.setCellValueFactory(new PropertyValueFactory<Flight, StringProperty>("origin"));
        destiny.setCellValueFactory(new PropertyValueFactory<Flight, StringProperty>("destiny"));
        terminal.setCellValueFactory(new PropertyValueFactory<Flight, IntegerProperty>("terminal"));
        plane.setCellValueFactory(new PropertyValueFactory<Flight, StringProperty>("plane"));
        status.setCellValueFactory(new PropertyValueFactory<Flight, ObjectProperty<FlightStatus>>("status"));
        delay.setCellValueFactory(new PropertyValueFactory<Flight, DoubleProperty>("delay"));
        contentPane.setItems(flights);
    }

    public void setNewAirportFlights(Collection<Flight> flightList) {
        flights.clear();
        addAirportFlights(flightList);
    }

    public void addAirportFlights(Collection<Flight> flightList) {
        for (Flight flight : flightList) {
            flights.add(flight);
            if (flight.isShared()) {
                for (SharedFlight sharedFlight : flight.getSharedFlights()) {
                    Flight sFlight = new Flight(sharedFlight.getFlightNumber(), flight.getDateTime().toLocalDate(), flight.getDateTime().toLocalTime(), flight.getFlightType());
                    sFlight.setCompany(sharedFlight.getCompany());
                    flights.add(sFlight);
                }
            }
        }
    }

    public void setVisibleFlights(ObservableList<Flight> flights) {
        this.flights = flights;
        contentPane.setItems(flights);
    }

}
