package dashboard.app.controller;

import dashboard.app.messages.MessageLogLevel;
import dashboard.app.messages.MessageLogger;
import dashboard.app.model.Airport;
import dashboard.app.model.AirportFlights;
import dashboard.app.model.FlightsCollection;
import dashboard.app.model.SummaryStats;
import dashboard.app.util.ADFXFileType;
import dashboard.app.util.FileIO;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by Mario on 04/03/2015.
 */
public class DataLoaderController extends AbstractModuleController<VBox> {

    @FXML
    private TextField folderPathTextField;

    @FXML
    private TextField numFlightsTextField;

    @FXML
    private TextField numAirportsTextField;

    @FXML
    private TextField fromTextField;

    @FXML
    private TextField toTextField;

    @FXML
    private ListView<File> filesListView;

    @FXML
    private ListView<File> selectedFilesListView;

    @FXML
    private Button selectFolderButton;

    @FXML
    private Button clearButton;

    @FXML
    private Button loadButton;

    @FXML
    private Button saveButton;

    @FXML
    private ToggleGroup saveOptionsGroup;

    @FXML
    private RadioButton noCompressionOption;

    @FXML
    private RadioButton zipCompressionOption;

    @FXML
    private RadioButton gzipCompressionOption;

    @FXML
    private ToggleGroup compressionOptionsGroup;

    @FXML
    private RadioButton singleFileSaveOption;

    @FXML
    private RadioButton oneFilePerAirportSaveOption;

    @FXML
    private RadioButton oneFilePerDaySaveOption;

    private ObservableList<File> files = FXCollections.observableArrayList();
    private ObservableList<File> selectedFiles = FXCollections.observableArrayList();
    private BooleanProperty dataLoaded = new SimpleBooleanProperty(false);
    private BooleanProperty filesSelected = new SimpleBooleanProperty(false);
    private SummaryStats summaryStats;
    private FileIO.SaveOption saveOption;
    private FileIO.CompressionOption compressionOption;
    private FlightsCollection flightsCollection;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        filesListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        filesListView.setCellFactory(param -> new AirportFlightsCellFactory());
        selectedFilesListView.setCellFactory(param -> new AirportFlightsCellFactory());
        filesListView.setItems(files);
        selectedFilesListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        selectedFilesListView.setItems(selectedFiles);
        File defaultDirectory = FileIO.getFile("data");
        folderPathTextField.setText(defaultDirectory.getPath());
        refreshFolder();
        selectedFiles.addListener((ListChangeListener<File>) c -> {
            filesSelected.set(!selectedFiles.isEmpty());
        });
        loadButton.disableProperty().bind(dataLoaded.or(filesSelected.not()));
        saveButton.disableProperty().bind(dataLoaded.not());
        clearButton.disableProperty().bind(dataLoaded.not());
        saveOptionsGroup.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {
            if (new_toggle != null) {
                saveOption = (new_toggle == singleFileSaveOption ?
                        FileIO.SaveOption.SINGLE_FILE :
                        saveOptionsGroup.getSelectedToggle() == oneFilePerAirportSaveOption ?
                                FileIO.SaveOption.ONE_FILE_PER_AIRPORT :
                                FileIO.SaveOption.ONE_FILE_PER_DAY);
            }
        });
        saveOptionsGroup.selectToggle(singleFileSaveOption);
        saveOption = FileIO.SaveOption.SINGLE_FILE;
        compressionOptionsGroup.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {
            if (new_toggle != null) {
                compressionOption = (new_toggle == noCompressionOption ?
                        FileIO.CompressionOption.NO_COMPRESSION :
                        compressionOptionsGroup.getSelectedToggle() == zipCompressionOption ?
                                FileIO.CompressionOption.ZIP :
                                FileIO.CompressionOption.GZIP);
            }
        });
        compressionOptionsGroup.selectToggle(singleFileSaveOption);
        compressionOption = FileIO.CompressionOption.NO_COMPRESSION;
    }

    @FXML
    private void selectFolderAction(ActionEvent event) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Chose a directory containing data");
//        File defaultDirectory = new File(System.getProperty("user.dir") + File.separator + Folders.DATASETS);
        File defaultDirectory = FileIO.getFile("data");
        directoryChooser.setInitialDirectory(defaultDirectory);
        //Show open file dialog
        File file = directoryChooser.showDialog(null);
        if (file != null) {
            folderPathTextField.setText(file.getPath());
            refreshFolder();
        }
    }

    private void refreshFolder() {
        String folderPath = folderPathTextField.getText();
        if (!folderPathTextField.getText().equals("")) {
            files.clear();
            File folder = new File(folderPath);
            List<File> fileList = FileIO.listFiles(folder, ADFXFileType.FLIGHTS.getFileTypeFilter(), false);
            files.addAll(fileList);
        }
    }

    @FXML
    private void selectSome(ActionEvent event) {
        List<File> selectedFileList = filesListView.getSelectionModel().getSelectedItems();
        selectedFiles.addAll(selectedFileList);
        files.removeAll(selectedFileList);
    }

    @FXML
    private void unselectSome(ActionEvent event) {
        List<File> selectedFileList = selectedFilesListView.getSelectionModel().getSelectedItems();
        files.addAll(selectedFileList);
        selectedFiles.removeAll(selectedFileList);
    }

    @FXML
    private void selectAll(ActionEvent event) {
        selectedFiles.addAll(files);
        files.clear();
    }

    @FXML
    private void unselectAll(ActionEvent event) {
        files.addAll(selectedFiles);
        selectedFiles.clear();
    }

    @FXML
    private void loadFlights(ActionEvent actionEvent) {
        if (selectedFiles.isEmpty()) return;
        flightsCollection = new FlightsCollection();
        for (File selectedFile : selectedFiles) {
            String extension = FileIO.getExtension(selectedFile);
            dashboard.data.jaxb.FlightsCollection fCollection = null;
            switch (extension) {
                case "xml":fCollection = (dashboard.data.jaxb.FlightsCollection) FileIO.unmarshallXML(selectedFile);
                    break;
                case "zip":fCollection = (dashboard.data.jaxb.FlightsCollection) FileIO.unmarshallZip(selectedFile);
                    break;
            }
            if (fCollection == null) continue;
            for (dashboard.data.jaxb.AirportFlights airportFlights : fCollection.getAirportFlights()) {

                AirportFlights af = new AirportFlights(airportFlights);
                flightsCollection.updateAirportFlights(af);
                MessageLogger.log(RecorderController.class, MessageLogLevel.LOW, String.format("Updated %s", af.getAirport().getName()));
            }
        }
        numAirportsTextField.textProperty().bind(Bindings.convert(flightsCollection.numAirportsProperty()));
        numFlightsTextField.textProperty().bind(Bindings.convert(flightsCollection.numFlightsProperty()));
        fromTextField.textProperty().bind(Bindings.convert(flightsCollection.fromProperty()));
        toTextField.textProperty().bind(Bindings.convert(flightsCollection.toProperty()));
        summaryStats = new SummaryStats(flightsCollection);
        dataLoaded.set(true);
    }

    @FXML
    private void saveFlights(ActionEvent actionEvent) {
        if (selectedFiles.isEmpty()) return;
        FileIO.saveToFile(flightsCollection, saveOption, compressionOption);
    }

    @FXML
    private void clearFlights(ActionEvent actionEvent) {
        selectedFiles.clear();
        refreshFolder();
        summaryStats = null;
        numAirportsTextField.clear();
        numFlightsTextField.clear();
        fromTextField.clear();
        toTextField.clear();
        dataLoaded.set(false);
    }

    public SummaryStats getSummaryStats() {
        return summaryStats;
    }

    public BooleanProperty dataLoadedProperty() {
        return dataLoaded;
    }

    public BooleanProperty filesSelectedProperty() {
        return filesSelected;
    }

    private static class AirportFlightsCellFactory extends ListCell<File> {
        HBox hbox = new HBox();
        Label airportLabel = new Label();

        public AirportFlightsCellFactory() {
            super();
            airportLabel.setPrefWidth(200);
            hbox.getChildren().addAll(airportLabel);
//            HBox.setHgrow(pane, Priority.ALWAYS);
        }

        @Override
        protected void updateItem(File item, boolean empty) {
            super.updateItem(item, empty);
            setText(null);  // No text in label of super class
            if (empty) {
                setGraphic(null);
            } else {
                String fileName = item.getName();
                airportLabel.setText(item.getName());
                setGraphic(hbox);
            }
        }
    }

}
