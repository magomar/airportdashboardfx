package dashboard.app.controller;

import dashboard.app.messages.MessageLogLevel;
import dashboard.app.messages.MessageLogger;
import dashboard.app.model.Airport;
import dashboard.app.model.AirportFlights;
import dashboard.app.model.FlightsCollection;
import dashboard.app.model.SummaryStats;
import dashboard.app.scrapping.FlightsProvider;
import dashboard.app.util.DateTimeUtils;
import dashboard.app.util.FileIO;
import dashboard.app.util.FixedExecutionRunnable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;

import java.io.File;
import java.net.URL;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by Mario on 12/02/2015.
 */
public class RecorderController implements Initializable {


    @FXML
    private ToggleGroup modeToggleGroup;

    @FXML
    private DatePicker fromDatePicker;

    @FXML
    private DatePicker toDatePicker;

    @FXML
    private ToggleButton recordToggleButton;

    @FXML
    private Button cancelToggleButton;

    @FXML
    private ToggleButton stopToggleButton;

    @FXML
    private ToggleButton programmedModeButton;

    @FXML
    private Slider saveFrequency;

    @FXML
    private Slider updateFrequency;

    @FXML
    private TextField fromTime;

    @FXML
    private TextField toTime;

    @FXML
    private HBox fromHBox;

    @FXML
    private HBox toHBox;

    @FXML
    private ToggleGroup saveOptionsGroup;

    @FXML
    private RadioButton singleFileSaveOption;

    @FXML
    private RadioButton oneFilePerAirportSaveOption;

    @FXML
    private RadioButton oneFilePerDaySaveOption;

    @FXML
    private RadioButton noCompressionOption;

    @FXML
    private RadioButton zipCompressionOption;

    @FXML
    private RadioButton gzipCompressionOption;

    @FXML
    private ToggleGroup compressionOptionsGroup;


    private FlightsCollection flightsCollection;
    private MultipleSelectionModel<Airport> airportSelectionModel;
    private FlightsProvider flightsProvider = FlightsProvider.AENA;
    private ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
    private BooleanProperty isRecording = new SimpleBooleanProperty(false);
    private boolean isProgrammed = false;
    private boolean isPaused = true;
    private List<Airport> airports;
    private LocalDateTime from;
    private LocalDateTime to;
    private SummaryStats summaryStats;
    private FileIO.SaveOption saveOption;
    private FileIO.CompressionOption compressionOption;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fromDatePicker.setValue(LocalDate.now());
        toDatePicker.setValue(LocalDate.now().plusDays(7));
        modeToggleGroup.selectToggle(stopToggleButton);
        saveOptionsGroup.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {
            saveOption = (new_toggle == singleFileSaveOption ?
                    FileIO.SaveOption.SINGLE_FILE :
                    saveOptionsGroup.getSelectedToggle() == oneFilePerAirportSaveOption ?
                            FileIO.SaveOption.ONE_FILE_PER_AIRPORT :
                            FileIO.SaveOption.ONE_FILE_PER_DAY);
        });
        saveOptionsGroup.selectToggle(singleFileSaveOption);
        saveOption = FileIO.SaveOption.SINGLE_FILE;
        compressionOptionsGroup.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {
            compressionOption = (new_toggle == noCompressionOption ?
                    FileIO.CompressionOption.NO_COMPRESSION :
                    compressionOptionsGroup.getSelectedToggle() == zipCompressionOption ?
                            FileIO.CompressionOption.ZIP :
                            FileIO.CompressionOption.GZIP);
        });
        compressionOptionsGroup.selectToggle(singleFileSaveOption);
        compressionOption = FileIO.CompressionOption.NO_COMPRESSION;
        saveFrequency.setValue(30);
    }

    private void updateFlights(Collection<Airport> airports, FlightsProvider flightsProvider) {
        MessageLogger.log(FlightsCollection.class, MessageLogLevel.MEDIUM, "Updating flights");
        List<Airport> airportsAvailable = getAirportsAvailable();
        List<Airport> targetAirports = new ArrayList<>();
        targetAirports.addAll(airports);
        targetAirports.retainAll(airportsAvailable);
        MessageLogger.log(FlightsCollection.class, MessageLogLevel.MEDIUM,
                String.format("Airports (available/selected): %d / %d ", targetAirports.size(), airports.size()));
        targetAirports
                .parallelStream()
                .map(flightsProvider::getAirportFlights)
                .map(this::getUpdatedAirportFlights);
    }

    private AirportFlights getUpdatedAirportFlights(CompletableFuture<AirportFlights> airportFlights) {
        try {
            AirportFlights af = airportFlights.get();
            flightsCollection.updateAirportFlights(af);
            MessageLogger.log(RecorderController.class, MessageLogLevel.LOW, String.format("Updated %s", af.getAirport().getName()));
            return flightsCollection.getAirportFlights(af.getAirport());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;

    }

    private List<Airport> getAirportsAvailable() {
        MessageLogger.log(FlightRecorderController.class, MessageLogLevel.MEDIUM, "Updating airports available...");
        List<Airport> airportsOnline = new ArrayList<>(50);
        try {
            airportsOnline = flightsProvider.getAirports().get();
            MessageLogger.log(FlightRecorderController.class, MessageLogLevel.MEDIUM,
                    String.format("Found %d airports", airportsOnline.size()));
        } catch (InterruptedException e) {
            MessageLogger.log(FlightRecorderController.class, MessageLogLevel.MEDIUM, "Error loading airports ");
        } catch (ExecutionException e) {
            MessageLogger.log(FlightRecorderController.class, MessageLogLevel.MEDIUM, "Error loading airports ");
        }
        return airportsOnline;
    }

    @FXML
    private void startRecording() {
        if (isPaused) {
            int cores = Runtime.getRuntime().availableProcessors();
            scheduledThreadPoolExecutor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(cores);
            if (!isRecording.get()) {
                flightsCollection = new FlightsCollection();
                airports = airportSelectionModel.getSelectedItems();
                MessageLogger.log(RecorderController.class, MessageLogLevel.HIGH, String.format("Recording flights for %d airports: %s", airports.size(), airports));
                if (isProgrammed) {
                    from = fromDatePicker.getValue().atTime(DateTimeUtils.parseTime(fromTime.getText()));
                    to = toDatePicker.getValue().atTime(DateTimeUtils.parseTime(toTime.getText()));
                    MessageLogger.log(RecorderController.class, MessageLogLevel.HIGH, String.format("Programmed from %s to %s\n", DateTimeUtils.format(flightsCollection.getFrom()),
                            DateTimeUtils.format(flightsCollection.getTo())));
                } else {
                    from = LocalDateTime.now();
                    MessageLogger.log(RecorderController.class, MessageLogLevel.HIGH, "Start recording");
                }
                summaryStats = new SummaryStats(flightsCollection);
                isRecording.set(true);
            } else {
                MessageLogger.log(RecorderController.class, MessageLogLevel.HIGH, "Resuming recording...");
            }
            if (isProgrammed) {
                LocalDateTime now = LocalDateTime.now();
                long delay;
                if (from.isBefore(now) || from.equals(now)) {
                    delay = 0;
                } else {
                    Duration duration = Duration.between(now, from);
                    delay = duration.toMinutes();
                }
//                if (to.isBefore(from)) to = from;
                runUntil(scheduledThreadPoolExecutor, () -> {
                            updateFlights(airports, flightsProvider);
                        },
                        to, delay, (long) updateFrequency.getValue(), TimeUnit.MINUTES);

                runUntil(scheduledThreadPoolExecutor, () -> {
                            FileIO.saveToFile(flightsCollection, saveOption, compressionOption);
                        },
                        to, (long) (updateFrequency.getValue() / 2),
                        delay + (long) updateFrequency.getValue(), TimeUnit.MINUTES
                );
            } else {
                scheduledThreadPoolExecutor.scheduleAtFixedRate(() -> {
                    updateFlights(airports, flightsProvider);
                }, 0, (long) updateFrequency.getValue(), TimeUnit.MINUTES);
                scheduledThreadPoolExecutor.scheduleAtFixedRate(() -> {
                    FileIO.saveToFile(flightsCollection, saveOption, compressionOption);
                }, (long) (updateFrequency.getValue() / 2), (long) saveFrequency.getValue(), TimeUnit.MINUTES);
            }
            isPaused = false;
        }
    }

    @FXML
    private void stopRecording() {
        if (isRecording.get() && !isPaused) {
            MessageLogger.log(RecorderController.class, MessageLogLevel.HIGH, "Entering pause...");
            updateFlights(airports, flightsProvider);
            scheduledThreadPoolExecutor.shutdown();
            try {
                scheduledThreadPoolExecutor.awaitTermination(5, TimeUnit.MINUTES);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                isPaused = true;
                MessageLogger.log(RecorderController.class, MessageLogLevel.HIGH, "Paused");
            }
        }
    }

    @FXML
    private void cancelRecording() {
        if (isRecording.get()) {
            MessageLogger.log(RecorderController.class, MessageLogLevel.HIGH, "Stopping...");
            if (!isPaused) {
                stopRecording();
                isPaused = false;
                modeToggleGroup.selectToggle(stopToggleButton);
            }

            FileIO.saveToFile(flightsCollection, saveOption, compressionOption);
            isRecording.set(false);
            MessageLogger.log(RecorderController.class, MessageLogLevel.HIGH, "Stopped");
        }
    }

    @FXML
    private void switchProgrammedMode() {
        isProgrammed = programmedModeButton.isSelected();
        if (isProgrammed) {
            fromHBox.setDisable(false);
            toHBox.setDisable(false);
            MessageLogger.log(RecorderController.class, MessageLogLevel.HIGH, "Programmed mode ON");
        } else {
            fromHBox.setDisable(true);
            toHBox.setDisable(true);
            MessageLogger.log(RecorderController.class, MessageLogLevel.HIGH, "Programmed mode OFF");
        }
    }

    public void setAirportSelectionModel(MultipleSelectionModel<Airport> airportSelectionModel) {
        this.airportSelectionModel = airportSelectionModel;
    }

    public BooleanProperty isRecordingProperty() {
        return isRecording;
    }

    public SummaryStats getSummaryStats() {
        return summaryStats;
    }

    public static void runUntil(ScheduledExecutorService executor, Runnable task, LocalDateTime maxRunTime, long initialDelay, long period, TimeUnit unit) {
        new FixedExecutionRunnable(task, maxRunTime).runUntil(executor, initialDelay, period, unit);
    }


}