package dashboard.app.controller;

import javafx.fxml.FXML;
import javafx.scene.Parent;

/**
 * Created by Mario on 04/03/2015.
 */
public abstract class AbstractModuleController<T extends Parent> implements ModuleController<T> {
    @FXML
    protected T contentPane;

    @Override
    public T getContentPane() {
        return contentPane;
    }
}
