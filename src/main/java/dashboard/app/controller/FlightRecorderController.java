package dashboard.app.controller;

import dashboard.app.messages.MessageLogLevel;
import dashboard.app.messages.MessageLogger;
import dashboard.app.messages.MessagesController;
import dashboard.app.model.*;
import dashboard.app.scrapping.FlightsProvider;
import dashboard.app.util.Settings;
import javafx.beans.property.BooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * Created by Mario on 08/02/2015.
 */
public class FlightRecorderController extends AbstractModuleController<BorderPane> implements Initializable {

    @FXML
    private ComboBox<Airport> airportComboBox;

    @FXML
    private ListView<Airport> airportListView;

    @FXML
    private FlightsController departuresController;

    @FXML
    private FlightsController arrivalsController;

    @FXML
    private ToggleButton recordToggleButton;

    @FXML
    private ToggleButton stopToggleButton;

    @FXML
    private ToggleButton cancelToggleButton;

    @FXML
    private ToggleGroup modeToggleButton;

    @FXML
    private MessagesController messagesController;

    @FXML
    private RecorderController recorderController;

    /**
     * Available airports
     */
    private ObservableList<Airport> airports = FXCollections.observableArrayList();

    /**
     * Map of airports indexed by code
     */
    private Map<String, Airport> airportMap = new HashMap<>();

    private final FlightsProvider flightsProvider = FlightsProvider.AENA;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        MessageLogger.initialize(messagesController, "flight-recorder");

        airports.addAll(AENA_Airport.values());

        for (Airport airport : airports) {
            airportMap.put(airport.getCode(), airport);
        }
        airportComboBox.setItems(airports);
        airportListView.setItems(airports);
        airportListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        String lastAirportShown = Settings.getInstance().getAirportShown();
        if (null != lastAirportShown) {
            airportComboBox.getSelectionModel().select(airportMap.get(lastAirportShown));
        } else {
            airportComboBox.getSelectionModel().select(0);
        }
        List<String> previousAirportsSelected = Settings.getInstance().getAirportSelected();
        if (!previousAirportsSelected.isEmpty()) {
            for (String airportCode : previousAirportsSelected) {
                airportListView.getSelectionModel().select(airportMap.get(airportCode));
            }
        }
        MessageLogger.log(FlightRecorderController.class, MessageLogLevel.MEDIUM, String.format("%d airports selected %s", previousAirportsSelected.size(), previousAirportsSelected));
        MultipleSelectionModel<Airport> airportSelectionModel = airportListView.getSelectionModel();
        recorderController.setAirportSelectionModel(airportSelectionModel);
        airportListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            Settings.getInstance().getAirportSelected().clear();
            for (Airport airport : airportSelectionModel.getSelectedItems()) {
                Settings.getInstance().getAirportSelected().add(airport.getCode());
            }
        });
    }

    @FXML
    private void showAirportFlights() {
        Airport airport = airportComboBox.getSelectionModel().getSelectedItem();
        Settings.getInstance().setAirportShown(airport.getCode());
        AirportFlights flightsMonitored = null;
        try {
            MessageLogger.log(FlightRecorderController.class, MessageLogLevel.MEDIUM, String.format("Loading flights from %s airport...", airport));
            flightsMonitored = flightsProvider.getAirportFlights(airport).get();
            MessageLogger.log(FlightRecorderController.class, MessageLogLevel.MEDIUM, String.format("Found %d departures and %d arrivals", flightsMonitored.getDepartures().getNumFlights(), flightsMonitored.getArrivals().getNumFlights()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Collection<Flight> departures = flightsMonitored.getDepartures().getFlights().values();
        departuresController.setNewAirportFlights(departures);

        Collection<Flight> arrivals = flightsMonitored.getArrivals().getFlights().values();
        arrivalsController.setNewAirportFlights(arrivals);
    }

    @FXML
    private void showMoreAirportFlights() {
        Airport airport = airportComboBox.getSelectionModel().getSelectedItem();
        Settings.getInstance().setAirportShown(airport.getCode());
        AirportFlights flightsMonitored = null;
        try {
            flightsMonitored = flightsProvider.getAirportFlights(airport).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Collection<Flight> departures = flightsMonitored.getDepartures().getFlights().values();
        departuresController.addAirportFlights(departures);

        Collection<Flight> arrivals = flightsMonitored.getArrivals().getFlights().values();
        arrivalsController.addAirportFlights(arrivals);

    }

    public BooleanProperty isRecordingProperty() {
        return recorderController.isRecordingProperty();
    }

    public SummaryStats getSummaryStats() {
        return recorderController.getSummaryStats();
    }
}
