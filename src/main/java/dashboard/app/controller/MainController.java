package dashboard.app.controller;

import javafx.beans.property.BooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * Created by Mario on 22/02/2015.
 */
public class MainController implements Initializable {

    private static final Logger LOG = Logger.getLogger(MainController.class.getName());

    private enum View {
        SPLASH("splash-screen.fxml"),
        FLIGHT_RECORDER("flight-recorder.fxml"),
        DATA_LOADER("data-loader.fxml"),
        SUMMARY("summary-stats.fxml"),
        AIRPORT_FLIGHTS("replay.fxml");
        private final String fxmlResourcePath;

        View(String fxmlFilename) {
            fxmlResourcePath = "/view/" + fxmlFilename;
        }
    }

    @FXML
    private Button airportStatsButton;

    @FXML
    private Button summaryStatsButton;

    @FXML
    private StackPane stackPane;

    private final Map<View, Parent> moduleViews = new HashMap<>(View.values().length);
    private final Map<View, ModuleController> moduleControllers = new HashMap<>(View.values().length);
    private View currentView;
    BooleanProperty dataLoaded;
    BooleanProperty isRecording;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (View view : View.values()) {
            loadModule(view);
        }
        loadSplashScreenView();

        dataLoaded = ((DataLoaderController) moduleControllers.get(View.DATA_LOADER)).dataLoadedProperty();
//        dataLoaded.addListener((observable, oldValue, newValue) -> {
//            if (newValue == true) loadSummaryView();
//        });
        isRecording = ((FlightRecorderController) moduleControllers.get(View.FLIGHT_RECORDER)).isRecordingProperty();
//        isRecording.addListener((observable, oldValue, newValue) -> {
//            if (newValue == true) loadSummaryView();
//        });
        summaryStatsButton.disableProperty().bind(dataLoaded.not().and(isRecording.not()));
        airportStatsButton.disableProperty().bind(dataLoaded.not().and(isRecording.not()));
    }

    @FXML
    private void loadSummaryView() {
        loadView(View.SUMMARY);
        SummaryStatsController moduleController = (SummaryStatsController) moduleControllers.get(View.SUMMARY);
        if (dataLoaded.get()) {
            DataLoaderController controller = (DataLoaderController) moduleControllers.get(View.DATA_LOADER);
            moduleController.setDataModel(controller.getSummaryStats());
        } else if (isRecording.get()) {
            FlightRecorderController controller = (FlightRecorderController) moduleControllers.get(View.FLIGHT_RECORDER);
            moduleController.setDataModel(controller.getSummaryStats());
        }

    }

    @FXML
    private void loadAirportFlightsView() {
        loadView(View.AIRPORT_FLIGHTS);
        ReplayController moduleController = (ReplayController) moduleControllers.get(View.AIRPORT_FLIGHTS);
        DataLoaderController dataController = (DataLoaderController) moduleControllers.get(View.DATA_LOADER);
        moduleController.setDataModel(dataController.getSummaryStats().getFlightsCollection());
    }

    @FXML
    private void loadSplashScreenView() {
        loadView(View.SPLASH);
        SplashScreenController controller = (SplashScreenController) moduleControllers.get(View.SPLASH);
        controller.resume();
    }

    @FXML
    private void loadDataLoaderView() {
        loadView(View.DATA_LOADER);
    }

    @FXML
    private void loadFlightRecorderView(ActionEvent actionEvent) {
        loadView(View.FLIGHT_RECORDER);
    }


    private void loadModule(View view) {
        if (moduleViews.containsKey(view)) return;
        else {
            FXMLLoader fxmlLoader = new FXMLLoader(MainController.class.getResource(view.fxmlResourcePath));
            Parent moduleContentPane = null;
            try {
                moduleContentPane = fxmlLoader.load();
                moduleContentPane.setOpacity(0);
                moduleViews.put(view, moduleContentPane);
                moduleControllers.put(view, fxmlLoader.getController());
                stackPane.getChildren().add(moduleContentPane);
            } catch (IOException e) {
                LOG.severe("Error loading module " + view.name());
                LOG.severe(e.getMessage());
            }

        }
    }

    private void loadView(View view) {
        SplashScreenController controller = (SplashScreenController) moduleControllers.get(View.SPLASH);
        controller.pause();
        if (currentView != null) moduleViews.get(currentView).setOpacity(0);
        currentView = view;
        Parent node = moduleViews.get(view);
        stackPane.getChildren().remove(node);
        stackPane.getChildren().add(node);
        node.setOpacity(1);
    }
}
