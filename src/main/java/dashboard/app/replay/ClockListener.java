package dashboard.app.replay;

import java.time.LocalDateTime;

/**
 * Created by Mario on 17/03/2015.
 */
public interface ClockListener {
    void update(LocalDateTime dateTime);
}
