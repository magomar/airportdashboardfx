package dashboard.app.scrapping;

import dashboard.app.model.Airport;
import dashboard.app.model.AirportFlights;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Created by Mario on 13/02/2015.
 */
public interface FlightsProvider {
    final static FlightsProvider AENA = new AENAScraper();

    CompletableFuture<List<Airport>> getAirports();

    CompletableFuture<AirportFlights> getAirportFlights(Airport airport);

}
