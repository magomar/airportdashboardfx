package dashboard.app.scrapping;

import dashboard.app.messages.MessageLogLevel;
import dashboard.app.messages.MessageLogger;
import dashboard.app.model.*;
import dashboard.app.util.DateTimeUtils;
import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

/**
 * Created by Mario on 13/02/2015.
 */
public class AENAScraper implements FlightsProvider {
    private final static String AENA_INFO_VUELOS_URL = "http://www.aena.es/csee/Satellite/infovuelos/es/";
    private final static Logger LOG = Logger.getLogger(AENAScraper.class.getName());

    @Override
    public CompletableFuture<AirportFlights> getAirportFlights(Airport airport) {
        return CompletableFuture.supplyAsync(() -> {
            AirportFlights airportFlights = null;
            try {
                airportFlights = new AirportFlights(airport);
                airportFlights.getDepartures().addFlights(loadFlights(airport, getDeparturesUrl(airport.getCode()), Flight.FlightType.DEPARTURE, true).get());
                airportFlights.getArrivals().addFlights(loadFlights(airport, getArrivalsUrl(airport.getCode()), Flight.FlightType.ARRIVAL, true).get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return airportFlights;
        });
    }

    @Override
    public CompletableFuture<List<Airport>> getAirports() {
        return CompletableFuture.supplyAsync(() -> {
            List<Airport> airports = new ArrayList<>();
            Document document = getDocument(AENA_INFO_VUELOS_URL);
            Element root = document.getElementById("origin_ac");
            Elements airportOptions = root.children();
            airportOptions.remove(0);
            for (Element element : airportOptions) {
                String text = element.text();
                String airportName = text.substring(0, text.indexOf('('));
                String airportCode = element.val();
                Airport aenaAirport = AENA_Airport.getAirportByCode(airportCode);
                Airport airport = (aenaAirport == null ? new SimpleAirport(airportName, airportCode) : aenaAirport);
                airports.add(airport);
            }
            return airports;
        });
    }

    private static CompletableFuture<FlightsHistory> loadFlights(Airport airport, String url, Flight.FlightType flightType, boolean detailed) {
        return CompletableFuture.supplyAsync(() -> {
            Document doc = getDocument(url);
            Element flightResults = null;
            FlightsHistory flightsHistory = new FlightsHistory();
            if (doc != null) {
                flightResults = doc.getElementById("flightResults");
                if (flightResults == null) return flightsHistory;
                Elements flightDates = flightResults.getElementsByClass("dateFlight");
                Elements tables = doc.select("table");
                int tableIndex = 0;
                Flight lastFlight = null;
                for (Element table : tables) {
                    String rawDateString = flightDates.get(tableIndex).text();
                    String[] words = rawDateString.split(",");
                    LocalDate localDate = DateTimeUtils.parseDate(words[1].trim());
                    Elements rows = table.select("tbody").select("tr");
                    List<Flight> flights = new ArrayList<>();
                    for (Element row : rows) {
                        String flightNumber = row.getElementsByAttributeValue("headers", "vuelo_" + tableIndex).text();
                        String time = row.getElementsByAttributeValue("headers", "hora_" + tableIndex).text();
                        boolean isASharedFlight = time.equals("");
                        String company = row.getElementsByAttributeValue("headers", "cia_" + tableIndex).text();
                        if (isASharedFlight) {
                            SharedFlight sharedFlight = new SharedFlight(flightNumber, company, lastFlight);
                            lastFlight.getSharedFlights().add(sharedFlight);
                        } else {
                            LocalTime localTime = DateTimeUtils.parseTime(time);
                            String terminal = row.getElementsByAttributeValue("headers", "terminal_" + tableIndex).text();
                            Flight flight;

                            if (flightType == Flight.FlightType.DEPARTURE) {
                                String destiny = row.getElementsByAttributeValue("headers", "destino_" + tableIndex).text();
                                flight = new Flight(flightNumber, localDate, localTime, flightType, airport.getName(), destiny);
                            } else {
                                String origin = row.getElementsByAttributeValue("headers", "destino_" + tableIndex).text();
                                flight = new Flight(flightNumber, localDate, localTime, flightType, origin, airport.getName());
                            }
                            flight.setCompany(company);
                            flight.setTerminal(terminal);
                            lastFlight = flight;
                            flights.add(flight);
                            if (detailed) {
                                Element flightLink = row.getElementsByClass("col2").get(0);
                                String flightUrl = flightLink.absUrl("href");
                                addFlightDetails(flightUrl, flight, flightType);
                            }
                        }
                    }
                    flightsHistory.addFlights(flights);
                    tableIndex++;
                }
            } else {
                if (flightType == Flight.FlightType.ARRIVAL)
                    MessageLogger.log(AENAScraper.class, MessageLogLevel.LOW, "Error loading arrivals for " + airport.toString());
                else
                    MessageLogger.log(AENAScraper.class, MessageLogLevel.LOW, "Error loading departures for " + airport.toString());
            }
            return flightsHistory;
        });
    }

    private static void addFlightDetails(String url, Flight flight, Flight.FlightType flightType) {
        CompletableFuture.supplyAsync(() -> {
                    Document doc = getDocument(url);
                    if (doc != null) {
                        Element txtAvion = doc.getElementsByClass("txtAvion").first();
                        String plane = txtAvion.text().split(":")[1];
                        flight.setPlane(plane);
                        Elements states = doc.getElementsByAttributeValueContaining("headers", "estado");
                        String statusMessage = null;
                        if (states.size() == 1) {
                            statusMessage = states.get(0).text();
                        } else {
                            switch (flightType) {
                                case DEPARTURE:
                                    statusMessage = states.get(0).text();
                                    break;
                                case ARRIVAL:
                                    statusMessage = states.get(1).text();
                                    break;
                            }
                        }
                        FlightStatusEvent flightStatusEvent = new FlightStatusEvent(LocalDateTime.now(), statusMessage, flight);
                        flight.updateStatusEvents(flightStatusEvent);
                    } else {
                        MessageLogger.log(AENAScraper.class, MessageLogLevel.LOW, "Error loading flight details at " + url);
                    }
                    return null;
                }
        );
    }

    private String getDeparturesUrl(String airportCode) {
        StringBuilder sb = new StringBuilder(AENA_INFO_VUELOS_URL);
        sb.append("?destiny=&mov=S&origin_ac=").append(airportCode);
        return sb.toString();
    }

    private String getArrivalsUrl(String airportCode) {
        StringBuilder sb = new StringBuilder(AENA_INFO_VUELOS_URL);
        sb.append("?origin=&mov=L&origin_ac=").append(airportCode);
        return sb.toString();
    }

    private static Document getDocument(String url) {
//        System.out.println("Fetching " + url);
        Validate.isTrue(url != null, "usage: supply url to fetch");
        Document doc = null;
        try {
            doc = Jsoup.connect(url).get();
            return doc;
        } catch (IOException e) {
        }
        return null;
    }
}
