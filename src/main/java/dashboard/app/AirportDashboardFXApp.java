package dashboard.app;

import dashboard.app.util.Settings;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *  Main application class
 *
 *  @author Mario G�mez Mart�nez
 */
public class AirportDashboardFXApp extends Application {
    private static final double MIN_WINDOW_WIDTH = 1024;
    private static final double MIN_WINDOW_HEIGHT = 800;
    private static final double PREFERRED_WINDOW_WIDTH = 1024;
    private static final double PREFERRED_WINDOW_HEIGHT = 800;

    static Stage PRIMARY_STAGE;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Settings.load();
        Parent root = FXMLLoader.load(getClass().getResource("/view/main.fxml"));

        this.PRIMARY_STAGE = primaryStage;
        primaryStage.setTitle("Airport Dashboard FX");
        primaryStage.setScene(new Scene(root));
        primaryStage.setMinWidth(MIN_WINDOW_WIDTH);
        primaryStage.setMinHeight(MIN_WINDOW_HEIGHT);
        primaryStage.setWidth(PREFERRED_WINDOW_WIDTH);
        primaryStage.setHeight(PREFERRED_WINDOW_HEIGHT);
//        URL appIcon = getClass().getClassLoader().getResource("images/app-icon-64.png");
        Image appIcon = new Image(getClass().getClassLoader().getResourceAsStream("images/app-icon-64.png"));
        primaryStage.getIcons().add(appIcon);
        primaryStage.show();
    }

    @Override
    public void stop() {
        Settings.getInstance().setWindowWidth(PRIMARY_STAGE.getWidth());
        Settings.getInstance().setWindowHeight(PRIMARY_STAGE.getHeight());
        Settings.save();
    }


}