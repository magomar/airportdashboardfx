package dashboard.app.model;

import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Mario on 08/03/2015.
 */
public class SummaryStats {

    private ObservableList<String> dateLabels = FXCollections.observableArrayList();
    private ObservableList<LocalDate> dates = FXCollections.observableArrayList();
    private ObservableList<AirportStats> airportStats = FXCollections.observableArrayList();
    private ObservableList<XYChart.Series<String, Integer>> flightsPerDaySeries = FXCollections.observableArrayList();
    private ObservableList<PieChart.Data> flightsPerAirportData = FXCollections.observableArrayList();
    private Map<LocalDate, FlightsCollection> flightsCollectionByDate = new TreeMap<>();
    private FlightsCollection flightsCollection;


    public SummaryStats(FlightsCollection flightsCollection) {
        this.flightsCollection = flightsCollection;
        update();

        flightsCollection.numFlightsProperty().addListener((observable, oldValue, newValue) -> {
            Platform.runLater(() -> update());
        });
    }

    public void update() {
        flightsCollectionByDate = flightsCollection.getFlightsCollectionByDate();
        XYChart.Series<String, Integer> flightsPerDayData = new XYChart.Series<>();
        XYChart.Series<String, Integer> nationalFlightsPerDayData = new XYChart.Series<>();
        XYChart.Series<String, Integer> internationalFlightsPerDayData = new XYChart.Series<>();
        flightsPerDaySeries.clear();
        dates.clear();
        dateLabels.clear();
        for (Map.Entry<LocalDate, FlightsCollection> entry : flightsCollectionByDate.entrySet()) {
            LocalDate date = entry.getKey();
            FlightsCollection fc = entry.getValue();
            dates.add(date);
            dateLabels.add(date.toString());
            flightsPerDayData.getData().add(new XYChart.Data<>(date.toString(), fc.getNumFlights()));
            flightsPerDayData.setName("Flights");
            nationalFlightsPerDayData.getData().add(new XYChart.Data<>(date.toString(), fc.getNumNationalFlights()));
            nationalFlightsPerDayData.setName("National");
            internationalFlightsPerDayData.getData().add(new XYChart.Data<>(date.toString(),
                    fc.getNumInternationalFlights()));
            internationalFlightsPerDayData.setName("International");
        }
        flightsPerDaySeries.addAll(flightsPerDayData, nationalFlightsPerDayData, internationalFlightsPerDayData);
        flightsPerAirportData.clear();
        flightsPerAirportData.addAll(flightsCollection.getAirportFlights().values()
                .stream()
                .map(airportFlights -> new PieChart.Data(airportFlights.getAirport().getCode(), airportFlights.getNumFlights()))
                .collect(Collectors.toList()));
        airportStats.clear();
        List<AirportFlights> airportFlightsList = new ArrayList<>(flightsCollection.getAirportFlights().values());
        Collections.sort(airportFlightsList, (o1, o2) -> o1.getAirport().getName().compareTo(o2.getAirport().getName()));
        airportStats.addAll(airportFlightsList
                .stream()
                .map(airportFlights -> new AirportStats(airportFlights, dates))
                .collect(Collectors.toList()));
    }

    public IntegerProperty numAirportsProperty() {
        return flightsCollection.numAirportsProperty();
    }

    public IntegerProperty numFlightsProperty() {
        return flightsCollection.numFlightsProperty();
    }

    public IntegerProperty numNationalFlightsProperty() {
        return flightsCollection.numNationalFlightsProperty();
    }

    public IntegerProperty numInternationalFlightsProperty() {
        return flightsCollection.numInternationalFlightsProperty();
    }

    public LocalDateTime getFrom() {
        return flightsCollection.getFrom();
    }

    public ObjectProperty<LocalDateTime> fromProperty() {
        return flightsCollection.fromProperty();
    }

    public LocalDateTime getTo() {
        return flightsCollection.getTo();
    }

    public ObjectProperty<LocalDateTime> toProperty() {
        return flightsCollection.toProperty();
    }

    public ObservableList<String> getDateLabels() {
        return dateLabels;
    }

    public ObservableList<AirportStats> getAirportStats() {
        return airportStats;
    }

    public Map<LocalDate, FlightsCollection> getFlightsCollectionByDate() {
        return flightsCollectionByDate;
    }

    public ObservableList<XYChart.Series<String, Integer>> getFlightsPerDaySeries() {
        return flightsPerDaySeries;
    }

    public ObservableList<PieChart.Data> getFlightsPerAirportData() {
        return flightsPerAirportData;
    }

    public FlightsCollection getFlightsCollection() {
        return flightsCollection;
    }

    public ObservableList<LocalDate> getDates() {
        return dates;
    }

}
