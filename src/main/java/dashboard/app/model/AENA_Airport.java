package dashboard.app.model;

/**
 * Enumeration of all the Spanish airports (AENA network)
 * @see <a href="http://www.aena.es/csee/Satellite/HomeAena">AENA</a>
 */
public enum AENA_Airport implements Airport {
    LCG("A Coruña"),
    MAD("Adolfo Suárez Madrid-Barajas"),
    ABC("Albacete"),
    AEI("Algeciras"),
    ALC("Alicante-Elche"),
    LEI("Almería"),
    OVD("Asturias"),
    BJZ("Badajoz"),
    BCN("Barcelona-El Prat"),
    BIO("Bilbao"),
    RGS("Burgos"),
    JCU("Ceuta"),
    ODB("Córdoba"),
    VDE("El Hierro"),
    FUE("Fuerteventura"),
    GRO("Girona-Costa Brava"),
    LPA("Gran Canaria"),
    GRX("Granada-Jaén F.G.L."),
    HSK("Huesca-Pirineos"),
    IBZ("Ibiza"),
    XRY("Jerez"),
    QGZ("La Gomera"),
    SPC("La Palma"),
    ACE("Lanzarote"),
    LEN("León"),
    RJL("Logroño-Agoncillo"),
    MCV("Madrid-Cuatro Vientos"),
    AGP("Málaga-Costa del Sol"),
    MLN("Melilla"),
    MAH("Menorca"),
    MJV("Murcia-San Javier"),
    PMI("Palma de Mallorca"),
    PNA("Pamplona"),
    REU("Reus"),
    QSA("Sabadell"),
    SLM("Salamanca"),
    EAS("San Sebastián"),
    SDR("Santander"),
    SCQ("Santiago"),
    SVQ("Sevilla"),
    SBO("Son Bonet"),
    TFN("Tenerife Norte"),
    TFS("Tenerife Sur"),
    VLC("Valencia"),
    VLL("Valladolid"),
    VGO("Vigo"),
    VIT("Vitoria"),
    ZAZ("Zaragoza");
    private final String fullName;

    private AENA_Airport(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public dashboard.data.jaxb.Airport getData() {
        dashboard.data.jaxb.Airport data = new dashboard.data.jaxb.Airport();
        data.setCode(name());
        data.setName(fullName);
        return data;
    }

    @Override
    public String getName() {
        return fullName;
    }

    @Override
    public String getCode() {
        return name();
    }

    @Override
    public String toString() {
        return fullName + " (" + name() + ')';
    }

    /**
     * Gets an instance of an airport given its code
     *
     * @param code the International Air Transport Association code for this airport
     * @return the instance of the airport with the given <code>code</code>, or null if no airport is found with that code
     * @see <a href="http://www.iata.org/publications/Pages/code-search.aspx">IATA airlines and airports codes</a>
     */
    public static Airport getAirportByCode(String code) {
        try {
            AENA_Airport aenaAirport = AENA_Airport.valueOf(code);
            return aenaAirport;
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
