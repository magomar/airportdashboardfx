package dashboard.app.model;

/**
 *
 * An airport described by a name and an airport code.
 */
public interface Airport extends DataWrapper<dashboard.data.jaxb.Airport> {
    /**
     *
     * @return the complete name of the airport
     */
    String getName();

    /**
     *
     * @return the international code of the airport, typically consisting of 3 letters
     */
    String getCode();
}
