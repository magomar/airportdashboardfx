package dashboard.app.model;

/**
 *
 * Any airport described a name and an airport code.
 */
public class SimpleAirport implements Airport {
    private String name;
    private String code;

    public SimpleAirport(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public SimpleAirport(dashboard.data.jaxb.Airport airport) {
        name = airport.getName();
        code = airport.getCode();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getCode() {
        return code;
    }


    @Override
    public String toString() {
        return name + " (" + code + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleAirport airport = (SimpleAirport) o;

        if (!code.equals(airport.code)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }

    public dashboard.data.jaxb.Airport getData() {
        dashboard.data.jaxb.Airport data = new dashboard.data.jaxb.Airport();
        data.setCode(code);
        data.setName(name);
        return data;
    }
}
