package dashboard.app.model;

/**
 * Created by Mario on 08/03/2015.
 */
public class UniqueFlight {
    private Flight arrival;
    private Flight departure;

    public Flight getArrival() {
        return arrival;
    }

    public void setArrival(Flight arrival) {
        this.arrival = arrival;
    }

    public Flight getDeparture() {
        return departure;
    }

    public void setDeparture(Flight departure) {
        this.departure = departure;
    }
}
