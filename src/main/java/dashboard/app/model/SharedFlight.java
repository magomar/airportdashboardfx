package dashboard.app.model;

/**
 * Data about a flight being operated by another company, with a different flight number.
 */
public class SharedFlight implements DataWrapper<dashboard.data.jaxb.SharedFlight> {
    private final String flightNumber;
    private final String company;
    private final Flight mainFlight;

    public SharedFlight(dashboard.data.jaxb.SharedFlight sharedFlight, Flight mainFlight) {
        flightNumber = sharedFlight.getFlightNumber();
        company = sharedFlight.getCompany();
        this.mainFlight = mainFlight;
    }

    public SharedFlight(String flightNumber, String company, Flight mainFlight) {
        this.flightNumber = flightNumber;
        this.company = company;
        this.mainFlight = mainFlight;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public String getCompany() {
        return company;
    }

    public Flight getMainFlight() {
        return mainFlight;
    }

    @Override
    public dashboard.data.jaxb.SharedFlight getData() {
        dashboard.data.jaxb.SharedFlight data = new dashboard.data.jaxb.SharedFlight();
        data.setFlightNumber(flightNumber);
        data.setCompany(company);
        return data;
    }
}
