package dashboard.app.model;

/**
 * Any class that is able a provide data of type {@link D}
 */
public interface DataWrapper<D> {
    /**
     *
     * @return an objet of type <code>D</code>
     */
    D getData();
}
