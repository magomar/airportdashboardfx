package dashboard.app.model;


import dashboard.app.util.DateTimeUtils;
import javafx.beans.property.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Mario on 09/02/2015.
 */
public class AirportFlights implements DataWrapper<dashboard.data.jaxb.AirportFlights> {
    private final Airport airport;
    private final FlightsHistory departures = new FlightsHistory();
    private final FlightsHistory arrivals = new FlightsHistory();
    private final IntegerProperty numFlights = new SimpleIntegerProperty(0);
    private final IntegerProperty numNationalFlights = new SimpleIntegerProperty(0);
    private final IntegerProperty numInternationalFlights = new SimpleIntegerProperty(0);
    private final ObjectProperty<LocalDateTime> from = new SimpleObjectProperty<>(LocalDateTime.MAX);
    private final ObjectProperty<LocalDateTime> to = new SimpleObjectProperty<>(LocalDateTime.MIN);
    private final DoubleProperty delay = new SimpleDoubleProperty();

    public AirportFlights(Airport airport) {
        trackChanges();
        this.airport = airport;
    }


    public AirportFlights(dashboard.data.jaxb.AirportFlights airportFlights) {
        trackChanges();
        String airportName = airportFlights.getAirport().getName();
        if (airportName.contains("("))
            airportName = airportName.substring(0, airportName.indexOf('(')).trim();
        String airportCode = airportFlights.getAirport().getCode();
        Airport aenaAirport = AENA_Airport.getAirportByCode(airportCode);
        airport = (aenaAirport == null ? new SimpleAirport(airportName, airportCode) : aenaAirport);
        if (airportFlights.getDepartures() != null) {
            departures.update(new FlightsHistory(airportFlights.getDepartures(), Flight.FlightType.DEPARTURE));
        }
        if (airportFlights.getArrivals() != null) {
            arrivals.update(new FlightsHistory(airportFlights.getArrivals(), Flight.FlightType.ARRIVAL));
        }
    }

    public void updateFlights(AirportFlights airportFlights) {
        departures.update(airportFlights.getDepartures());
        arrivals.update(airportFlights.getArrivals());
    }

    private void trackChanges() {
        numFlights.bind(arrivals.numFlightsProperty().add(departures.numFlightsProperty()));
        numNationalFlights.bind(arrivals.numNationalFlightsProperty().add(departures.numNationalFlightsProperty()));
        numInternationalFlights.bind(arrivals.numInternationalFlightsProperty().add(departures.numInternationalFlightsProperty()));
        departures.fromProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isBefore(from.get()))
                from.set(newValue);
        });
        arrivals.fromProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isBefore(from.get()))
                from.set(newValue);
        });
        departures.toProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isAfter(to.get()))
                to.set(newValue);
        });
        arrivals.toProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isAfter(to.get()))
                to.set(newValue);
        });
        delay.bind(arrivals.delayProperty().add(departures.delayProperty()).divide(2.0));
    }

    public Airport getAirport() {
        return airport;
    }

    public FlightsHistory getDepartures() {
        return departures;
    }

    public FlightsHistory getArrivals() {
        return arrivals;
    }

    public double getDelay() {
        return delay.get();
    }

    public DoubleProperty delayProperty() {
        return delay;
    }

    public int getNumNationalFlights() {
        return numNationalFlights.get();
    }

    public IntegerProperty numNationalFlightsProperty() {
        return numNationalFlights;
    }

    public int getNumInternationalFlights() {
        return numInternationalFlights.get();
    }

    public IntegerProperty numInternationalFlightsProperty() {
        return numInternationalFlights;
    }

    public int getNumFlights() {
        return numFlights.get();
    }

    public IntegerProperty numFlightsProperty() {
        return numFlights;
    }

    public LocalDateTime getFrom() {
        return from.get();
    }

    public ObjectProperty<LocalDateTime> fromProperty() {
        return from;
    }

    public LocalDateTime getTo() {
        return to.get();
    }

    public ObjectProperty<LocalDateTime> toProperty() {
        return to;
    }

    @Override
    public dashboard.data.jaxb.AirportFlights getData() {
        dashboard.data.jaxb.AirportFlights data = new dashboard.data.jaxb.AirportFlights();
        data.setAirport(airport.getData());
        data.setDepartures(departures.getData());
        data.setArrivals(arrivals.getData());
        return data;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("AirportFlights for ").append(airport.toString()).append('\n');
        sb.append("- From: ").append(DateTimeUtils.format(from.get()));
        sb.append("- To: ").append(DateTimeUtils.format(to.get())).append('\n');
        sb.append("- Departures: ").append(departures).append('\n');
        sb.append("- Arrivals: ").append(arrivals).append('\n');
        return sb.toString();
    }

    public Map<LocalDate, AirportFlights> getAirportFlightsByDate() {

        Map<LocalDate, AirportFlights> airportFlightsByDate = new TreeMap<>();
        LocalDate dateFrom = from.get().toLocalDate();
        LocalDate dateTo = to.get().toLocalDate();
        LocalDate dateBetween = LocalDate.from(dateFrom);
        while (dateBetween.isBefore(dateTo) || dateBetween.isEqual(dateTo)) {
            AirportFlights fc = new AirportFlights(airport);
            airportFlightsByDate.put(dateBetween, fc);
            dateBetween = dateBetween.plusDays(1);
        }
        Map<LocalDate, FlightsHistory> departuresByDate = departures.getFlightsHistoryByDate();
        Map<LocalDate, FlightsHistory> arrivalsByDate = arrivals.getFlightsHistoryByDate();
        for (LocalDate date : airportFlightsByDate.keySet()) {
            AirportFlights af = new AirportFlights(airport);
            if (arrivalsByDate.containsKey(date))
                af.getArrivals().addFlights(arrivalsByDate.get(date));
            if (departuresByDate.containsKey(date))
                af.getDepartures().addFlights(departuresByDate.get(date));
            airportFlightsByDate.get(date).updateFlights(af);
        }
        return airportFlightsByDate;
    }
}
