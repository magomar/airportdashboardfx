package dashboard.app.model;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Collection of flights sorted by date and time
 */
public class FlightsHistory implements DataWrapper<dashboard.data.jaxb.FlightsHistory> {
    private ObservableMap<String, Flight> flights = FXCollections.observableMap(new TreeMap<>());
    private IntegerProperty numFlights = new SimpleIntegerProperty(0);
    private IntegerProperty numNationalFlights = new SimpleIntegerProperty(0);
    private IntegerProperty numInternationalFlights = new SimpleIntegerProperty(0);
    private ObjectProperty<LocalDateTime> from = new SimpleObjectProperty<>(LocalDateTime.MAX);
    private ObjectProperty<LocalDateTime> to = new SimpleObjectProperty<>(LocalDateTime.MIN);
    private DoubleProperty delay = new SimpleDoubleProperty(0.0);
    private DoubleBinding delayBinding = new SimpleDoubleProperty(0.0).add(0.0);

    public FlightsHistory() {
    }

    public FlightsHistory(dashboard.data.jaxb.FlightsHistory flightsHistory, Flight.FlightType flightType) {
        for (dashboard.data.jaxb.Flight flight : flightsHistory.getFlight()) {
            addFlight(new Flight(flight, flightType));
        }
    }

    public void update(FlightsHistory flightsHistory) {
        for (Map.Entry<String, Flight> entry : flightsHistory.flights.entrySet()) {
            String id = entry.getKey();
            Flight flight = entry.getValue();
            if (!addFlight(flight)) {
                Flight f = flights.get(id);
                f.updateStatusEvents(flight.getFlightStatus().getFlightStatusEvents());
            }
        }
    }

    public void addFlights(FlightsHistory flightsHistory) {
        addFlights(flightsHistory.flights.values());
    }

    public void addFlights(Collection<Flight> someFlights) {
        someFlights.forEach(this::addFlight);
    }

    public boolean addFlight(Flight flight) {
        String id = flight.getId();
        if (!flights.containsKey(id)) {
            flights.put(id, flight);
            LocalDateTime dateTime = flight.getDateTime();
            if (dateTime.isBefore(from.get())) from.set(dateTime);
            if (dateTime.isAfter(to.get())) to.set(dateTime);
            numFlights.set(numFlights.get() + 1);
            if (flight.getFlightDomain() == Flight.FlightDomain.NATIONAL) {
                numNationalFlights.set(numNationalFlights.get() + 1);
            } else {
                numInternationalFlights.set(numInternationalFlights.get() + 1);
            }
            delayBinding = delayBinding.add(flight.delayProperty());
            delay.bind(delayBinding.divide(numFlightsProperty()));
            return true;
        } else return false;
    }


    public ObservableMap<String, Flight> getFlights() {
        return flights;
    }

    public double getDelay() {
        return delay.get();
    }

    public DoubleProperty delayProperty() {
        return delay;
    }

    public LocalDateTime getFrom() {
        return from.get();
    }

    public ObjectProperty<LocalDateTime> fromProperty() {
        return from;
    }

    public LocalDateTime getTo() {
        return to.get();
    }

    public ObjectProperty<LocalDateTime> toProperty() {
        return to;
    }

    public int getNumFlights() {
        return numFlights.get();
    }

    public IntegerProperty numFlightsProperty() {
        return numFlights;
    }

    public int getNumNationalFlights() {
        return numNationalFlights.get();
    }

    public IntegerProperty numNationalFlightsProperty() {
        return numNationalFlights;
    }

    public int getNumInternationalFlights() {
        return numInternationalFlights.get();
    }

    public IntegerProperty numInternationalFlightsProperty() {
        return numInternationalFlights;
    }

    @Override
    public dashboard.data.jaxb.FlightsHistory getData() {
        dashboard.data.jaxb.FlightsHistory data = new dashboard.data.jaxb.FlightsHistory();
        data.getFlight().addAll(flights.values()
                .stream()
                .map(Flight::getData)
                .collect(Collectors.toList()));
        return data;
    }

    @Override
    public String toString() {
        if (flights.isEmpty()) return "";
        StringBuilder sb = new StringBuilder('[');
        for (Flight flight : flights.values()) {
            sb.append(flight.getFlightNumber()).append(", ");
        }
        if (sb.length() > 1)
            sb.delete(sb.length() - 2, sb.length() - 1);
        sb.append(']');
        return sb.toString();
    }

    public ObservableMap<LocalDate, FlightsHistory> getFlightsHistoryByDate() {
        ObservableMap<LocalDate, FlightsHistory> flightsByDate = FXCollections.observableMap(new TreeMap<>());
        for (Flight flight : flights.values()) {
            LocalDate date = flight.getDate();
            if (flightsByDate.containsKey(date)) flightsByDate.get(date).addFlight(flight);
            else {
                FlightsHistory flightsHistory = new FlightsHistory();
                flightsHistory.addFlight(flight);
                flightsByDate.put(date, flightsHistory);
            }
        }
        for (FlightsHistory flightsHistory : flightsByDate.values()) {
            flightsHistory.numFlights.set(flightsHistory.flights.size());
        }
        return flightsByDate;
    }
}
