package dashboard.app.model;

import dashboard.app.util.DateTimeUtils;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Holds all the information about a single flight, which is uniquely identified by an id.
 */
public class Flight implements Comparable<Flight>, DataWrapper<dashboard.data.jaxb.Flight> {
    private final String id;
    private final String flightNumber;
    private final LocalDateTime dateTime;
    private String origin;
    private String destiny;
    private String plane;
    private String company;
    private String terminal;
    private List<SharedFlight> sharedFlights = new LinkedList<>();
    private final FlightType flightType;
    private FlightDomain flightDomain;
    private FlightStatus flightStatus;


    public Flight(dashboard.data.jaxb.Flight flight, FlightType flightType) {
        flightNumber = flight.getFlightNumber();
        this.flightType = flightType;
        LocalDate date = DateTimeUtils.parseDate(flight.getDate());
        LocalTime time = DateTimeUtils.parseTime(flight.getTime());
        dateTime = LocalDateTime.of(date, time);
        id = obtainId(flightNumber, date, time);
        setOriginAndDeparture(flight.getOrigin(), flight.getDestiny());
        plane = flight.getPlane();
        company = flight.getCompany();
        terminal = flight.getTerminal();
        sharedFlights.addAll(flight.getSharedFlight()
                .stream()
                .map(sharedFlight -> new SharedFlight(sharedFlight, this))
                .collect(Collectors.toList()));
        flightStatus = new FlightStatus(dateTime, flightType, this);
        flightStatus.getFlightStatusEvents().addAll(
                flight.getStatusEvent()
                        .stream()
                        .map((statusEvent) -> new FlightStatusEvent(statusEvent, this))
                        .collect(Collectors.toList()));
    }

    public Flight(String flightNumber, LocalDate date, LocalTime time, FlightType flightType) {
        this.flightNumber = flightNumber;
        this.flightType = flightType;
        this.dateTime = LocalDateTime.of(date, time);
        id = obtainId(flightNumber, date, time);
        flightStatus = new FlightStatus(dateTime, flightType, this);
    }

    public Flight(String flightNumber, LocalDate date, LocalTime time, FlightType flightType, String origin, String destiny) {
        this.flightNumber = flightNumber;
        this.flightType = flightType;
        this.dateTime = LocalDateTime.of(date, time);
        id = obtainId(flightNumber, date, time);
        setOriginAndDeparture(origin, destiny);
        flightStatus = new FlightStatus(dateTime, flightType, this);
    }

    public Flight(String flightNumber, LocalDateTime dateTime, FlightType flightType, String origin, String destiny) {
        this.flightNumber = flightNumber;
        this.flightType = flightType;
        this.dateTime = dateTime;
        id = obtainId(flightNumber, dateTime);
        setOriginAndDeparture(origin, destiny);
        flightStatus = new FlightStatus(dateTime, flightType, this);
    }

    private void setOriginAndDeparture(String origin, String destiny) {
        this.origin = origin;
        this.destiny = destiny;
        flightDomain = getFlightDomain(origin, destiny, flightType);
}

    public void setPlane(String plane) {
        this.plane = plane;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public FlightStatus getFlightStatus() {
        return flightStatus;
    }

    public boolean isShared() {
        return !sharedFlights.isEmpty();
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public LocalDate getDate() {
        return dateTime.toLocalDate();
    }

    public LocalTime getTime() {
        return dateTime.toLocalTime();
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestiny() {
        return destiny;
    }

    public String getPlane() {
        return plane;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getTerminal() {
        return terminal;
    }

    public FlightType getFlightType() {
        return flightType;
    }

    public FlightDomain getFlightDomain() {
        return flightDomain;
    }

    public void setSharedFlights(List<SharedFlight> sharedFlights) {
        this.sharedFlights = sharedFlights;
    }

    public void setFlightDomain(FlightDomain flightDomain) {
        this.flightDomain = flightDomain;
    }

    public List<SharedFlight> getSharedFlights() {
        return sharedFlights;
    }

    public String getId() {
        return id;
    }

    public double getDelay() {
        return flightStatus.getDelay();
    }

    public DoubleProperty delayProperty() {
        return flightStatus.delayProperty();
    }

    public FlightStatusEvent getStatus() {
        return flightStatus.getStatus();
    }

    public ObjectProperty<FlightStatusEvent> statusProperty() {
        return flightStatus.statusProperty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Flight flight = (Flight) o;

        if (!id.equals(flight.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Flight{" +
                "flightNumber=" + flightNumber +
                ", date=" + dateTime.toLocalDate() +
                ", time=" + dateTime.toLocalTime() +
                ", origin=" + origin +
                ", destiny=" + destiny +
                ", company=" + company +
                ", terminal=" + terminal +
                ", plane=" + plane +
                ", flight-status=" + flightStatus +
                '}';
    }

    @Override
    public dashboard.data.jaxb.Flight getData() {
        dashboard.data.jaxb.Flight data = new dashboard.data.jaxb.Flight();
        data.setFlightNumber(flightNumber);
        data.setDate(DateTimeUtils.format(dateTime.toLocalDate()));
        data.setTime(DateTimeUtils.format(dateTime.toLocalTime()));
        data.setOrigin(origin);
        data.setDestiny(destiny);
        data.setCompany(company);
        data.setTerminal(terminal);
        data.setPlane(plane);
        List<dashboard.data.jaxb.SharedFlight> sharedFlightList = data.getSharedFlight();
        sharedFlightList.addAll(sharedFlights.stream().map(SharedFlight::getData).collect(Collectors.toList()));
        List<dashboard.data.jaxb.StatusEvent> statusEventList = data.getStatusEvent();
        statusEventList.addAll(flightStatus.getFlightStatusEvents().stream().map(FlightStatusEvent::getData).collect(Collectors.toList()));
        return data;
    }

    @Override
    public int compareTo(Flight o) {
        int dateComparison = getDate().compareTo(o.getDate());
        if (dateComparison != 0) return dateComparison;
        int timeComparison = getTime().compareTo(o.getTime());
        if (timeComparison != 0) return timeComparison;
        return flightNumber.compareTo(o.flightNumber);

    }

    public void updateStatusEvents(Collection<FlightStatusEvent> flightStatusEvents) {
        flightStatus.getFlightStatusEvents().addAll(flightStatusEvents);
    }

    public void updateStatusEvents(FlightStatusEvent flightStatusEvents) {
        flightStatus.getFlightStatusEvents().add(flightStatusEvents);
    }

    /**
     * Whether a flight is national or international
     */
    public enum FlightDomain {
        NATIONAL,
        INTERNATIONAL;
    }

    /**
     * Whether a flight is a departure or an arrival
     */
    public static enum FlightType {
        DEPARTURE,
        ARRIVAL;
    }

    private static FlightDomain getFlightDomain(String origin, String destiny, FlightType flightType) {
        return (flightType == FlightType.ARRIVAL ? getArrivalDomain(origin) : getDepartureDomain(destiny));
    }

    private static FlightDomain getArrivalDomain(String origin) {
        String airportCode = origin.substring(origin.indexOf('(') + 1, origin.indexOf(')'));
        if (AENA_Airport.getAirportByCode(airportCode) != null) return FlightDomain.NATIONAL;
        else return FlightDomain.INTERNATIONAL;
    }

    private static FlightDomain getDepartureDomain(String destiny) {
        String airportCode = destiny.substring(destiny.indexOf('(') + 1, destiny.indexOf(')'));
        if (AENA_Airport.getAirportByCode(airportCode) != null) return FlightDomain.NATIONAL;
        else return FlightDomain.INTERNATIONAL;
    }

    private static String obtainId(String flightNumber, LocalDate date, LocalTime time) {
        return DateTimeUtils.format(date) + '_' + DateTimeUtils.format(time) + '_' + flightNumber;
    }

    private static String obtainId(String flightNumber, LocalDateTime dateTime) {
        return DateTimeUtils.format(dateTime.toLocalDate()) + '_' + DateTimeUtils.format(dateTime.toLocalTime()) + '_' + flightNumber;
    }
}
