package dashboard.app.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import javafx.collections.SetChangeListener;

import java.time.LocalDateTime;
import java.util.TreeSet;

/**
 * The current status and history of events of a flight
 */
public class FlightStatus {
    private ObservableSet<FlightStatusEvent> flightStatusEvents = FXCollections.observableSet(new TreeSet<>());
    private ObjectProperty<FlightStatusEvent> status = new SimpleObjectProperty<>();
    private DoubleProperty delay = new SimpleDoubleProperty(0);

    public FlightStatus(LocalDateTime programmedDateTime, Flight.FlightType flightType, Flight flight) {
        FlightStatusEvent initialEvent = null;
        switch (flightType) {
            case ARRIVAL:
                initialEvent = new FlightStatusEvent(programmedDateTime.minusHours(6), FlightStatusType.ESTIMATED_ARRIVAL, flight.getDateTime(), flight);
                break;
            case DEPARTURE:
                initialEvent = new FlightStatusEvent(programmedDateTime.minusHours(6), FlightStatusType.ESTIMATED_DEPARTURE, flight.getDateTime(), flight);
                break;

        }
        status.addListener((observable, oldValue, newValue) ->
                        delay.set(newValue.getDelay())
        );
        status.set(initialEvent);
        flightStatusEvents.addListener((SetChangeListener<FlightStatusEvent>) change -> {
            FlightStatusEvent fse = change.getElementAdded();
            FlightStatusEvent currentFlightStatusEvent = status.get();
            if (currentFlightStatusEvent == null) status.set(fse);
            else if (currentFlightStatusEvent.compareTo(fse) < 0)
                status.set(fse);
        });
    }

    public ObservableSet<FlightStatusEvent> getFlightStatusEvents() {
        return flightStatusEvents;
    }

    public FlightStatusEvent getStatus() {
        return status.get();
    }

    public ObjectProperty<FlightStatusEvent> statusProperty() {
        return status;
    }

    public double getDelay() {
        return delay.get();
    }

    public DoubleProperty delayProperty() {
        return delay;
    }

    @Override
    public String toString() {
        return "FlightStatus{" +
                "flightStatusEvents=" + flightStatusEvents +
                ", status=" + status +
                ", delay=" + delay +
                '}';
//        return status.get().toString();
    }


}
