package dashboard.app.model;

import dashboard.app.replay.ClockListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * Created by Mario on 17/03/2015.
 */
public class AirportReplayModel implements ClockListener {
    private FlightsReplayModel arrivals;
    private FlightsReplayModel departures;

    public AirportReplayModel(AirportFlights airportFlights) {
        arrivals = new FlightsReplayModel(airportFlights.getArrivals());
        departures = new FlightsReplayModel(airportFlights.getDepartures());
    }

    private static Flight copyFlightWithoutStatus(Flight flight) {
        Flight f = new Flight(flight.getFlightNumber(), flight.getDateTime(), flight.getFlightType(), flight.getOrigin(), flight.getDestiny());
        f.setFlightDomain(flight.getFlightDomain());
        f.setCompany(flight.getCompany());
        f.setTerminal(flight.getTerminal());
        f.setPlane(flight.getPlane());
        f.setSharedFlights(flight.getSharedFlights());
        return f;
    }

    public ObservableList<Flight> getVisibleArrivals() {
        return arrivals.visibleFlights;
    }

    public ObservableList<Flight> getVisibleDepartures() {
        return departures.visibleFlights;
    }

    @Override
    public void update(LocalDateTime dateTime) {
        arrivals.updateDateTime(dateTime);
        departures.updateDateTime(dateTime);
    }

    private class FlightsReplayModel {
        private PriorityQueue<FlightStatusEvent> events;
        private ObservableList<Flight> visibleFlights = FXCollections.observableArrayList();
        private Map<String, Flight> flights;

        public FlightsReplayModel(FlightsHistory flightsHistory) {
            events = new PriorityQueue<>(flightsHistory.getNumFlights() * 2, FlightStatusEvent.TIME_STAMP_COMPARATOR);
            flights = new HashMap<>(flightsHistory.getNumFlights());
            for (Flight flight : flightsHistory.getFlights().values()) {
                Flight f = copyFlightWithoutStatus(flight);
                flights.put(f.getId(), f);
                events.addAll(flight.getFlightStatus().getFlightStatusEvents());
            }
        }

        public void updateDateTime(LocalDateTime dateTime) {
            FlightStatusEvent event = events.peek();
            LocalDateTime timeStamp = event.getTimeStamp();
            while (event != null && (timeStamp.isBefore(dateTime) || timeStamp.isEqual(dateTime))) {
                Flight flight = flights.get(event.getFlight().getId());
                if (!visibleFlights.contains(flight)) visibleFlights.add(flight);
                flight.updateStatusEvents(event);
                events.remove();
                event = events.peek();
                timeStamp = event.getTimeStamp();
            }
        }
    }

}
