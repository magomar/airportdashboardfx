package dashboard.app.model;

import dashboard.app.util.DateTimeUtils;
import dashboard.data.jaxb.StatusEvent;
import dashboard.data.jaxb.StatusType;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Comparator;

/**
 * A single status event. Events occur when there is change in the status of a flight. For example, a flight which is
 * scheduled actually lands or takes off, or the estimated time of arrival/departure changes.
 */
public class FlightStatusEvent implements DataWrapper<StatusEvent>, Comparable<FlightStatusEvent> {
    private final LocalDateTime timeStamp;
    private final FlightStatusType type;
    private final LocalDateTime dateTime;
    private final DoubleProperty delay = new SimpleDoubleProperty(0);
    private final Flight flight;
    public static final Comparator<FlightStatusEvent> TIME_STAMP_COMPARATOR = (o1, o2) ->
            (!o1.timeStamp.equals(o2.timeStamp) ?
                    o1.timeStamp.compareTo(o2.timeStamp) : o1.dateTime.compareTo(o2.dateTime));

    public FlightStatusEvent(LocalDateTime timeStamp, FlightStatusType type, LocalDateTime dateTime, Flight flight) {
        this.timeStamp = timeStamp;
        this.type = type;
        this.dateTime = dateTime;
        this.flight = flight;
        delay.set(Duration.between(flight.getDateTime(), dateTime).toMinutes());
    }

    public FlightStatusEvent(dashboard.data.jaxb.StatusEvent statusEvent, Flight flight) {
        this(DateTimeUtils.parseDateTime(statusEvent.getTimeStamp()),
                FlightStatusType.valueOf(statusEvent.getType().name()),
                computeDateTime(flight.getDateTime(), DateTimeUtils.parseTime(statusEvent.getTime())),
                flight);
        delay.set(Duration.between(flight.getDateTime(), dateTime).toMinutes());
    }

    public FlightStatusEvent(LocalDateTime timeStamp, String message, Flight flight) {
        this(timeStamp,
                FlightStatusType.getFlightStatusTypeFromMessage(message),
                computeDateTime(flight.getDateTime(), DateTimeUtils.parseTime(message.substring(message.length() - 5, message.length()))),
                flight);
        delay.set(Duration.between(flight.getDateTime(), dateTime).toMinutes());
    }

    private static LocalDateTime computeDateTime(LocalDateTime programmedDateTime, LocalTime dateTime) {
        long delayInMinutes = Duration.between(programmedDateTime.toLocalTime(), dateTime).toMinutes();
        LocalDateTime actualDateTime;
        if (delayInMinutes >= -15) {
            actualDateTime = LocalDateTime.of(programmedDateTime.toLocalDate(), dateTime);
        } else {
            LocalDate actualDate = programmedDateTime.toLocalDate().plusDays(1);
            actualDateTime = LocalDateTime.of(actualDate, dateTime);
        }
        return actualDateTime;
    }

    public Flight getFlight() {
        return flight;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public String getMessage() {
        return type.getShortMsg() + DateTimeUtils.format(dateTime.toLocalTime());
    }

    public FlightStatusType getType() {
        return type;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public double getDelay() {
        return delay.get();
    }

    public DoubleProperty delayProperty() {
        return delay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FlightStatusEvent that = (FlightStatusEvent) o;

        if (type != that.type) return false;
        if (!dateTime.equals(that.dateTime)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + dateTime.hashCode();
        return result;
    }

    @Override
    public int compareTo(FlightStatusEvent o) {
        if (!type.equals(o.type)) return type.ordinal() - o.type.ordinal();
        return dateTime.compareTo(o.dateTime);
    }

    // TODO save dateTime instead of only the time

    @Override
    public StatusEvent getData() {
        StatusEvent data = new StatusEvent();
        data.setTimeStamp(DateTimeUtils.format(timeStamp));
        if (type != null)
            data.setType(StatusType.valueOf(type.name()));
        if (dateTime != null)
            data.setTime(DateTimeUtils.format(dateTime.toLocalTime()));
        return data;
    }

    @Override
    public String toString() {
        return getMessage();
    }
}
