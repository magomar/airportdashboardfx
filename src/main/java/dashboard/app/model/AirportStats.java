package dashboard.app.model;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;

import java.time.LocalDate;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Mario on 11/03/2015.
 */
public class AirportStats {
    private final ObjectProperty<Airport> airport = new SimpleObjectProperty<>();
    private final StringProperty airportName = new SimpleStringProperty();
    private final StringProperty airportFullName = new SimpleStringProperty();
    private final StringProperty airportCode = new SimpleStringProperty();
    private final DoubleProperty flightsDay = new SimpleDoubleProperty();
    //TODO probably dates field is not needed
    private final ObservableList<LocalDate> dates;
    private final AirportFlights airportFlights;
    private ObservableList<XYChart.Series<String, Integer>> flightsPerDayData = FXCollections.observableArrayList();
    private Map<LocalDate, AirportFlights> airportFlightsByDate = new TreeMap<>();
    private ObservableList<String> dateLabels = FXCollections.observableArrayList();


    public AirportStats(AirportFlights airportFlights, ObservableList<LocalDate> dates) {
        this.airportFlights = airportFlights;
        this.dates = dates;
        Airport a = airportFlights.getAirport();
        airport.set(a);
        airportName.set(a.getName());
        airportCode.set(a.getCode());
        airportFullName.set(airport.toString());
        dates.addListener(new ListChangeListener<LocalDate>() {
            @Override
            public void onChanged(Change<? extends LocalDate> c) {
                flightsDay.bind(AirportStats.this.airportFlights.numFlightsProperty().divide((double) dates.size()));
            }
        });
        flightsDay.bind(airportFlights.numFlightsProperty().divide((double) dates.size()));

    }

    public void update() {
        airportFlightsByDate = airportFlights.getAirportFlightsByDate();
        XYChart.Series<String, Integer> flightsPerDaySeries = new XYChart.Series<>();
        XYChart.Series<String, Integer> nationalFlightsPerDaySeries = new XYChart.Series<>();
        XYChart.Series<String, Integer> internationalFlightsPerDaySeries = new XYChart.Series<>();
        flightsPerDayData.clear();
        dateLabels.clear();
        for (Map.Entry<LocalDate, AirportFlights> entry : airportFlightsByDate.entrySet()) {
            LocalDate date = entry.getKey();
            AirportFlights af = entry.getValue();
            dateLabels.add(date.toString());
            flightsPerDaySeries.getData().add(new XYChart.Data<>(date.toString(), af.getNumFlights()));
            flightsPerDaySeries.setName("Flights");
            nationalFlightsPerDaySeries.getData().add(new XYChart.Data<>(date.toString(), af.getNumNationalFlights()));
            nationalFlightsPerDaySeries.setName("National");
            internationalFlightsPerDaySeries.getData().add(new XYChart.Data<>(date.toString(),
                    af.getNumInternationalFlights()));
            internationalFlightsPerDaySeries.setName("International");
        }
        flightsPerDayData.addAll(flightsPerDaySeries, nationalFlightsPerDaySeries, internationalFlightsPerDaySeries);
    }

    public String getAirportFullName() {
        return airportFullName.get();
    }

    public StringProperty airportFullNameProperty() {
        return airportFullName;
    }

    public String getAirportCode() {
        return airportCode.get();
    }

    public StringProperty airportCodeProperty() {
        return airportCode;
    }

    public String getAirportName() {
        return airportName.get();
    }

    public StringProperty airportNameProperty() {
        return airportName;
    }

    public int getNumFlights() {
        return airportFlights.getNumFlights();
    }

    public IntegerProperty numFlightsProperty() {
        return airportFlights.numFlightsProperty();
    }

    public Airport getAirport() {
        return airport.get();
    }

    public ObjectProperty<Airport> airportProperty() {
        return airport;
    }

    public AirportFlights getAirportFlights() {
        return airportFlights;
    }

    public int getNumNationalFlights() {
        return airportFlights.getNumNationalFlights();
    }

    public IntegerProperty numNationalFlightsProperty() {
        return airportFlights.numNationalFlightsProperty();
    }

    public int getNumInternationalFlights() {
        return airportFlights.getNumInternationalFlights();
    }

    public IntegerProperty numInternationalFlightsProperty() {
        return airportFlights.numInternationalFlightsProperty();
    }

    public double getFlightsDay() {
        return flightsDay.get();
    }

    public DoubleProperty flightsDayProperty() {
        return flightsDay;
    }

    public double getAverageDelay() {
        return airportFlights.delayProperty().get();
    }

    public DoubleProperty averageDelayProperty() {
        return airportFlights.delayProperty();
    }

    public ObservableList<LocalDate> getDates() {
        return dates;
    }

    public ObservableList<XYChart.Series<String, Integer>> getFlightsPerDayData() {
        return flightsPerDayData;
    }

    public ObservableList<String> getDateLabels() {
        return dateLabels;
    }
}

