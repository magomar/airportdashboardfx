package dashboard.app.messages;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Mario on 10/02/2015.
 */
public class MessagesController implements Initializable {
    private static final int MAX_MESSAGES = 200;

    @FXML
    private ListView<String> messagesListView;

    @FXML
    private ToggleGroup logLevelToggleGroup;

    @FXML
    private RadioButton verboseRadioButton;

    @FXML
    private RadioButton regularRadioButton;

    @FXML
    private RadioButton minimalRadioButton;

    @FXML
    private RadioButton offRadioButton;

    private ObservableList<String> messages = FXCollections.observableArrayList();
    private MessageLogLevel logLevel = MessageLogLevel.LOW;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        messagesListView.setItems(messages);
        logLevelToggleGroup.selectToggle(verboseRadioButton);
        logLevel = MessageLogLevel.LOW;
    }

    public synchronized void addMessage(String message) {
        messages.add(message);
        if (messages.size() > MAX_MESSAGES) messages.remove(0);
    }

    public void clear() {
        messages.clear();
    }

    @FXML
    private void switchLogLevel(ActionEvent event) {
        RadioButton radioButton = (RadioButton) logLevelToggleGroup.getSelectedToggle();
        switch (radioButton.getId()) {
            case "verboseRadioButton":
                logLevel = MessageLogLevel.LOW;
                break;
            case "minimalRadioButton":
                logLevel = MessageLogLevel.HIGH;
                break;
            case "offRadioButton":
                logLevel = MessageLogLevel.OFF;
                break;
            default:
                logLevel = MessageLogLevel.MEDIUM;
        }
    }

    public MessageLogLevel getLogLevel() {
        return logLevel;
    }

}
