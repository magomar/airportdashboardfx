package dashboard.app.messages;

/**
 * Created by Mario on 23/02/2015.
 */

import javafx.application.Platform;

import java.util.logging.ErrorManager;
import java.util.logging.LogRecord;
import java.util.logging.StreamHandler;

public class MessagesHandler extends StreamHandler {

    private MessagesController messagesController;

    public MessagesHandler(MessagesController messagesController) {
        super();
        this.messagesController = messagesController;
        setFormatter(new MessageFormatter());
        setOutputStream(System.out);
    }

    @Override
    public synchronized void publish(LogRecord record) {
        if (!isLoggable(record)) {
            return;
        }
        if (record.getLevel().intValue() >= messagesController.getLogLevel().intValue())
            try {
                log(getFormatter().format(record));
            } catch (Exception ex) {
                // We don't want to throw an exception here, but we
                // report the exception to any registered ErrorManager.
                reportError(null, ex, ErrorManager.FORMAT_FAILURE);
                return;
            }
    }


    @Override
    public void flush() {
        messagesController.clear();
    }


    private void log(String msg) {
        Platform.runLater(() -> messagesController.addMessage(msg));
    }
}