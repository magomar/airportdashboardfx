package dashboard.app.messages;

import java.util.logging.Level;

/**
 * Created by Mario on 24/02/2015.
 */
public class MessageLogLevel extends Level {

    public static final MessageLogLevel OFF = new MessageLogLevel("OFF", Integer.MAX_VALUE);
    public static final MessageLogLevel HIGH = new MessageLogLevel("HIGH", Level.SEVERE.intValue());
    public static final MessageLogLevel MEDIUM = new MessageLogLevel("MEDIUM", Level.WARNING.intValue());
    public static final MessageLogLevel LOW = new MessageLogLevel("LOW", Level.INFO.intValue());
    public static final MessageLogLevel ALL = new MessageLogLevel("ALL", Integer.MIN_VALUE);

    protected MessageLogLevel(String name, int value) {
        super(name, value);
    }

    protected MessageLogLevel(String name, int value, String resourceBundleName) {
        super(name, value, resourceBundleName);
    }

}
