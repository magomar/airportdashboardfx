package dashboard.app.messages;

import dashboard.app.util.FileIO;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

/**
 * Created by Mario on 23/02/2015.
 */
public class MessageLogger {

    private static MessagesHandler messagesHandler;
    private static FileHandler fileHandler;
    private static Map<Class, Logger> loggerCache = new HashMap<>();
    private final static MessageLogLevel logLevel = MessageLogLevel.LOW;

    private MessageLogger() {
    }

    public static void initialize(MessagesController messagesController, String appName) {
        messagesHandler = new MessagesHandler(messagesController);
//        try {
//            fileHandler = new FileHandler("log/" + appName + "-log.%u.%g.txt");
//        } catch (IOException e) {
//            try {
//                fileHandler = new FileHandler("../log/" + appName + "-log.%u.%g.txt");
//            } catch (IOException e1) {
//                e1.printStackTrace();
//            }
//        }
        try {
            if (FileIO.isDeployed())
                fileHandler = new FileHandler("../log/" + appName + "-log.%u.%g.txt");
            else
                fileHandler = new FileHandler("log/" + appName + "-log.%u.%g.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Logger getLogger(Class whoLogs) {
        if (loggerCache.containsKey(whoLogs)) return loggerCache.get(whoLogs);
        else {
            Logger logger = Logger.getLogger(whoLogs.getName());
            logger.setLevel(logLevel);
            if (messagesHandler != null) logger.addHandler(messagesHandler);
            if (fileHandler != null) logger.addHandler(fileHandler);
            loggerCache.put(whoLogs, logger);
            return logger;
        }
    }

    public static void log(Class whoLogs, MessageLogLevel level, String message) {
        getLogger(whoLogs).log(level, message);
    }

}
