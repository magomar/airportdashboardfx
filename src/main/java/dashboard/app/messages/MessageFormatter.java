package dashboard.app.messages;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * Created by Mario on 23/02/2015.
 */
public class MessageFormatter extends Formatter {
    @Override
    public String format(LogRecord record) {
        String levelStr = "";
        int levelValue = record.getLevel().intValue();
        if (levelValue >= MessageLogLevel.HIGH.intValue()) levelStr = ">";
        else if (levelValue >= MessageLogLevel.MEDIUM.intValue()) levelStr = ">>";
        else if (levelValue >= MessageLogLevel.LOW.intValue()) levelStr = ">>>";
        return String.format("%s " + levelStr + " %s",
                millisToLocalDateTime(record.getMillis()).format(DATE_TIME_FORMATTER),
                record.getMessage());
    }

    private static LocalDateTime millisToLocalDateTime(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        return LocalDateTime.of(year, month, day, hours, minutes, seconds);
    }

    static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyy HH:mm:ss");
}
