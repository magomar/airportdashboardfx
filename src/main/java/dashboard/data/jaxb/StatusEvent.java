package dashboard.data.jaxb;

import javax.xml.bind.annotation.*;


/**
 * <p>Clase Java para StatusEvent complex type.
 * <p>
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;complexType name="StatusEvent">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="time-stamp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="type" type="{}StatusType"/>
 *         &lt;element name="time" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatusEvent", propOrder = {
        "timeStamp",
        "type",
        "time"
})
public class StatusEvent {

    @XmlElement(name = "time-stamp", required = true)
    protected String timeStamp;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected StatusType type;
    @XmlElement(required = true)
    protected String time;

    /**
     * Obtiene el valor de la propiedad timeStamp.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTimeStamp(String value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad type.
     *
     * @return possible object is
     * {@link StatusType }
     */
    public StatusType getType() {
        return type;
    }

    /**
     * Define el valor de la propiedad type.
     *
     * @param value allowed object is
     *              {@link StatusType }
     */
    public void setType(StatusType value) {
        this.type = value;
    }

    /**
     * Obtiene el valor de la propiedad time.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTime() {
        return time;
    }

    /**
     * Define el valor de la propiedad time.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTime(String value) {
        this.time = value;
    }

}
