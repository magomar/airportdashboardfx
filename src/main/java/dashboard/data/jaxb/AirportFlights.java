package dashboard.data.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AirportFlights complex type.
 * <p>
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;complexType name="AirportFlights">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="airport" type="{}Airport"/>
 *         &lt;element name="arrivals" type="{}FlightsHistory"/>
 *         &lt;element name="departures" type="{}FlightsHistory"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirportFlights", propOrder = {
        "airport",
        "arrivals",
        "departures"
})
public class AirportFlights {

    @XmlElement(required = true)
    protected Airport airport;
    @XmlElement(required = true)
    protected FlightsHistory arrivals;
    @XmlElement(required = true)
    protected FlightsHistory departures;

    /**
     * Obtiene el valor de la propiedad airport.
     *
     * @return possible object is
     * {@link Airport }
     */
    public Airport getAirport() {
        return airport;
    }

    /**
     * Define el valor de la propiedad airport.
     *
     * @param value allowed object is
     *              {@link Airport }
     */
    public void setAirport(Airport value) {
        this.airport = value;
    }

    /**
     * Obtiene el valor de la propiedad arrivals.
     *
     * @return possible object is
     * {@link FlightsHistory }
     */
    public FlightsHistory getArrivals() {
        return arrivals;
    }

    /**
     * Define el valor de la propiedad arrivals.
     *
     * @param value allowed object is
     *              {@link FlightsHistory }
     */
    public void setArrivals(FlightsHistory value) {
        this.arrivals = value;
    }

    /**
     * Obtiene el valor de la propiedad departures.
     *
     * @return possible object is
     * {@link FlightsHistory }
     */
    public FlightsHistory getDepartures() {
        return departures;
    }

    /**
     * Define el valor de la propiedad departures.
     *
     * @param value allowed object is
     *              {@link FlightsHistory }
     */
    public void setDepartures(FlightsHistory value) {
        this.departures = value;
    }

}
