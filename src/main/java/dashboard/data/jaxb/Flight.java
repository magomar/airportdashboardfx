package dashboard.data.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para Flight complex type.
 * <p>
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;complexType name="Flight">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="flight-number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="time" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="origin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="destiny" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="plane" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="passengers" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="terminal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shared-flight" type="{}SharedFlight" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="status-event" type="{}StatusEvent" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Flight", propOrder = {
        "flightNumber",
        "date",
        "time",
        "origin",
        "destiny",
        "plane",
        "company",
        "passengers",
        "terminal",
        "sharedFlight",
        "statusEvent"
})
public class Flight {

    @XmlElement(name = "flight-number", required = true)
    protected String flightNumber;
    @XmlElement(required = true)
    protected String date;
    @XmlElement(required = true)
    protected String time;
    protected String origin;
    protected String destiny;
    @XmlElement(required = true)
    protected String plane;
    protected String company;
    protected Integer passengers;
    protected String terminal;
    @XmlElement(name = "shared-flight")
    protected List<SharedFlight> sharedFlight;
    @XmlElement(name = "status-event", required = true)
    protected List<StatusEvent> statusEvent;

    /**
     * Obtiene el valor de la propiedad flightNumber.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Define el valor de la propiedad flightNumber.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad date.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDate() {
        return date;
    }

    /**
     * Define el valor de la propiedad date.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDate(String value) {
        this.date = value;
    }

    /**
     * Obtiene el valor de la propiedad time.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTime() {
        return time;
    }

    /**
     * Define el valor de la propiedad time.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTime(String value) {
        this.time = value;
    }

    /**
     * Obtiene el valor de la propiedad origin.
     *
     * @return possible object is
     * {@link String }
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Define el valor de la propiedad origin.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setOrigin(String value) {
        this.origin = value;
    }

    /**
     * Obtiene el valor de la propiedad destiny.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDestiny() {
        return destiny;
    }

    /**
     * Define el valor de la propiedad destiny.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDestiny(String value) {
        this.destiny = value;
    }

    /**
     * Obtiene el valor de la propiedad plane.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPlane() {
        return plane;
    }

    /**
     * Define el valor de la propiedad plane.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPlane(String value) {
        this.plane = value;
    }

    /**
     * Obtiene el valor de la propiedad company.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCompany() {
        return company;
    }

    /**
     * Define el valor de la propiedad company.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCompany(String value) {
        this.company = value;
    }

    /**
     * Obtiene el valor de la propiedad passengers.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getPassengers() {
        return passengers;
    }

    /**
     * Define el valor de la propiedad passengers.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setPassengers(Integer value) {
        this.passengers = value;
    }

    /**
     * Obtiene el valor de la propiedad terminal.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTerminal() {
        return terminal;
    }

    /**
     * Define el valor de la propiedad terminal.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTerminal(String value) {
        this.terminal = value;
    }

    /**
     * Gets the value of the sharedFlight property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sharedFlight property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSharedFlight().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SharedFlight }
     */
    public List<SharedFlight> getSharedFlight() {
        if (sharedFlight == null) {
            sharedFlight = new ArrayList<SharedFlight>();
        }
        return this.sharedFlight;
    }

    /**
     * Gets the value of the statusEvent property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the statusEvent property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatusEvent().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusEvent }
     */
    public List<StatusEvent> getStatusEvent() {
        if (statusEvent == null) {
            statusEvent = new ArrayList<StatusEvent>();
        }
        return this.statusEvent;
    }

}
