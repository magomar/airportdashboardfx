package dashboard.data.jaxb;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para StatusType.
 * <p>
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="StatusType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ESTIMATED_DEPARTURE"/>
 *     &lt;enumeration value="ESTIMATED_ARRIVAL"/>
 *     &lt;enumeration value="DEPARTED"/>
 *     &lt;enumeration value="ARRIVED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "StatusType")
@XmlEnum
public enum StatusType {

    ESTIMATED_DEPARTURE,
    ESTIMATED_ARRIVAL,
    DEPARTED,
    ARRIVED;

    public String value() {
        return name();
    }

    public static StatusType fromValue(String v) {
        return valueOf(v);
    }

}
